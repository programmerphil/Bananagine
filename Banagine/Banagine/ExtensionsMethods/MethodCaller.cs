﻿using System;
using System.Linq;
using System.Reflection;

namespace Banagine.ExtensionsMethods
{
    public class MethodCaller
    {

        public object Call(object callObject, string methodName, BindingFlags bindingFlags, Func<MethodInfo, Type[], bool> methodMatchCondition, params object[] parameters)
        {
            Type callObjectType = callObject.GetType();
            foreach (var publicMethod in callObjectType.GetMethods(bindingFlags))
            {
                Type[] parameterTypes = parameters.Select(item => item.GetType()).ToArray();
                if (publicMethod.Name == methodName)
                {
                    if (methodMatchCondition(publicMethod, parameterTypes))
                    {
                        return publicMethod.Invoke(callObject, parameters);
                    }
                }
            }

            return null;
        }

        public object Call(object callObject, string methodName, BindingFlags bindingFlags, params object[] parameters)
        {
            return Call(callObject, methodName, bindingFlags, DoesAllParametersMatch, parameters);
        }

        public bool DoesAllParametersMatch(MethodInfo method, Type[] parameterTypes)
        {
            Type[] methodParameterTypes = method.GetParameters().Select(item => item.ParameterType).ToArray();
            bool doesAllParametersMatch = true;

            for (int i = 0; i < methodParameterTypes.Length; i++)
            {
                if (methodParameterTypes[i] != parameterTypes[i])
                {
                    doesAllParametersMatch = false;
                }
            }
            return doesAllParametersMatch;
        }


        public T Call<T>(object callObject, string methodName, BindingFlags bindingFlags, params object[] parameters)
        {
            return (T)Call(callObject, methodName, bindingFlags, DoesAllParametersMatch, parameters);
        }

        public T Call<T>(object callObject, string methodName, BindingFlags bindingFlags,Func<MethodInfo, Type[], bool> methodMatchCondition, params object[] parameters)
        {
            return (T)Call(callObject, methodName, bindingFlags, methodMatchCondition, parameters);
        }
    }
}