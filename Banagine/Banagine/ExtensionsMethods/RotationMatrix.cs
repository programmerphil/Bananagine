﻿using System;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.ExtensionsMethods
{
    public static class RotationMatrix
    {
        public static Matrix<float> Create(float angle)
        {
            System.Console.WriteLine("Degrees " + angle);
            angle *= ((float)Math.PI / 180);
            System.Console.WriteLine("Radians " + angle);
            Matrix<float> rotationMatrix = Matrix<float>.Build.Dense(2, 2);
            rotationMatrix[0, 0] = (float)Math.Cos(angle);
            rotationMatrix[1, 0] = (float)Math.Sin(angle);
            rotationMatrix[0, 1] = -(float)Math.Sin(angle);
            rotationMatrix[1, 1] = (float)Math.Cos(angle);

            return rotationMatrix;
        }
    }
}