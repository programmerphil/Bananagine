﻿using System;

namespace Banagine.ExtensionsMethods
{
    public static class FloatExtensions
    {
        private const double Epsilon = 1e-10;

        public static bool IsZero(this float d)
        {
            return Math.Abs(d) < Epsilon;
        }
    }
}