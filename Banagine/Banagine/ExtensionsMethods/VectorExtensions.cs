﻿using MathNet.Numerics.LinearAlgebra;

namespace Banagine.ExtensionsMethods
{
    public static class VectorExtensions
    {
        public static float GetX(this Vector<float> vector)
        {
            return vector[0];
        }

        public static float GetY(this Vector<float> vector)
        {
            return vector[1];
        }

        public static float SetX(this Vector<float> vector, float x)
        {
            return vector[0] = x;
        }

        public static float SetY(this Vector<float> vector, float y)
        {
            return vector[1] = y;
        }

        public static Vector<float> PerpendicularClockwise(this Vector<float> vector2)
        {
            return Vector2Builder.Create(-vector2.GetY(), vector2.GetX());
        }

        public static Vector<float> PerpendicularCounterClockwise(this Vector<float> vector2)
        {
            return Vector2Builder.Create(vector2.GetY(), -vector2.GetX());
        }

        public static float Cross(this Vector<float> vector2, Vector<float> otherVector2)
        {
             return vector2.GetX() * otherVector2.GetY() - vector2.GetY() * otherVector2.GetX();
        }
    }
}