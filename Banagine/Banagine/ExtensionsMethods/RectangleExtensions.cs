﻿using System.Drawing;

namespace Banagine.ExtensionsMethods
{
    public static class RectangleExtensions
    {
        public static RectangleF Divide(this Rectangle rectangle, Size value)
        {
            return new RectangleF((float)rectangle.X / value.Width, (float)rectangle.Y / value.Height, (float)rectangle.Size.Width / value.Width, (float)rectangle.Size.Height / value.Height);
        }
    }
}