﻿using System.Collections.Generic;

namespace Banagine.ExtensionsMethods
{
    public static class ListExtensions
    {
        public static void AddIfNotExist<T>(this List<T> list, T item)
        {
            if (!list.Contains(item))
            {
                list.Add(item);
            }
        }

        public static void RemoveRange<T>(this List<T> list, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                list.Remove(item);
            }
        }
    }
}