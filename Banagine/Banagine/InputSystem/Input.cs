﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Banagine.EC;
using Banagine.EventSystem;
using Banagine.ExtensionsMethods;

namespace Banagine.InputSystem
{
    public class Input : IEventReceiver, IEventsFinder
    {
        private List<IPullInputManager> _pullInputManagers;
        private List<IPushInputManager> _pushInputManagers;

        public IEnumerable<EventData> EventDatas => new EventData[]
        {
            new EventData<InputEventArgs>(OnRequestedInput, EngineEvent.GetInput),
            new EventData<EventArgs>(EarlyUpdate, EngineEvent.EarlyUpdate),
            new EventData<EventArgs>(LateUpdate, EngineEvent.LateUpdate),
        };

        public Input(List<IPullInputManager> pullInputManagers, List<IPushInputManager> pushInputManagers)
        {
            _pullInputManagers = pullInputManagers;
            _pushInputManagers = pushInputManagers;
        }

        public void OnRequestedInput(object sender, InputEventArgs e)
        {
            foreach (var inputManager in _pullInputManagers)
            {
                EventArgs output = inputManager.GetInput(e.Input);
                if (output != null)
                {
                    EventManager.SendMessage(this, EngineEvent.InputChecked, output);
                }
            }
        }

        public void EarlyUpdate(object sender, EventArgs e)
        {
            foreach (var pullInputManager in _pullInputManagers)
            {
                (pullInputManager as IEarlyUpdateable)?.EarlyUpdate();
            }
            foreach (var pushInputManager in _pushInputManagers)
            {
                pushInputManager.Push();
            }
        }

        public void LateUpdate(object sender, EventArgs e)
        {
            foreach (var pullInputManager in _pullInputManagers)
            {
                (pullInputManager as ILateUpdateable)?.LateUpdate();
            }
        }

        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            return new IEventReceiver[]{ this};
        }
    }
}