﻿using System;
using System.Reflection;
using System.Windows.Input;
using Banagine.ExtensionsMethods;

namespace Banagine.InputSystem
{
    public interface IPullInputManager
    {
        EventArgs GetInput(object inputObject);
    }

    public abstract class PullInputManager<TOut, TIn> : IPullInputManager
    {
        private const string GetInputMethodName = "GetInput";

        private MethodCaller _methodCaller;

        protected PullInputManager()
        {
            _methodCaller = new MethodCaller();
        }

        public EventArgs GetInput(object inputObject)
        {
            return
                new OutputEventArgs<TOut>((TOut)_methodCaller.Call(this, GetInputMethodName,
                    BindingFlags.Instance | BindingFlags.NonPublic, DoesReturnValueMatch,
                    inputObject));
        }

        private bool DoesReturnValueMatch(MethodInfo method, Type[] parameterTypes)
        {
            return method.ReturnType == typeof(TOut) && _methodCaller.DoesAllParametersMatch(method, parameterTypes);
        }

        protected abstract TOut GetInput(TIn inputEnum);
    }

    public abstract class PullInputManager<TOut> : IPullInputManager
    {
        public EventArgs GetInput(object inputObject)
        {
            return new OutputEventArgs<TOut>(GetInput());
        }

        protected abstract TOut GetInput();
    }
}