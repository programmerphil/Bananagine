﻿using System.Windows.Input;

namespace Banagine.InputSystem
{
    public class KeyInputData
    {
        public KeyState KeyState;
        public Key Key;

        public KeyInputData(KeyState keyState, Key key)
        {
            KeyState = keyState;
            Key = key;
        }
    }
}