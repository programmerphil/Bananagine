﻿using System.Windows.Forms;
using Banagine.EC;
using Banagine.EventSystem;
using Gma.System.MouseKeyHook;

namespace Banagine.InputSystem
{
    public class MousePushInputManager : IPushInputManager
    {
        private IKeyboardMouseEvents _applicationHook;
        private MouseEventExtArgs _lastMouseEventExtArgs;
        private MouseEventExtArgs _newMouseEventExtArgs;

        public MousePushInputManager()
        {
            _applicationHook = Hook.AppEvents();
            _applicationHook.MouseDownExt += AppHookMouseDown;
            _applicationHook.MouseUpExt += AppHookMouseDown;
            _applicationHook.MouseMoveExt += AppHookMouseDown;
        }

        private void AppHookMouseDown(object sender, MouseEventExtArgs e)
        {
            _newMouseEventExtArgs = e;
        }

        public void Push()
        {
            bool isOldMouseEventButtonDown = (_lastMouseEventExtArgs?.IsMouseButtonDown).GetValueOrDefault();
            bool isNewMouseEventButtonDown = (_newMouseEventExtArgs?.IsMouseButtonDown).GetValueOrDefault();

            if (isNewMouseEventButtonDown != isOldMouseEventButtonDown)
            {
                if (isNewMouseEventButtonDown)
                {
                    EventManager.SendMessage(this, MouseEvent.MouseDown, _newMouseEventExtArgs);
                }
                else
                {
                    EventManager.SendMessage(this, MouseEvent.MouseUp, _newMouseEventExtArgs);
                }
               
            }

            if (isNewMouseEventButtonDown == isOldMouseEventButtonDown && isNewMouseEventButtonDown)
            {
                EventManager.SendMessage(this, MouseEvent.MousePressed, _newMouseEventExtArgs);
            }

            _lastMouseEventExtArgs = _newMouseEventExtArgs;
        }
    }
}