﻿namespace Banagine.InputSystem
{
    public interface IPushInputManager
    {
        void Push();
    }
}