﻿namespace Banagine.InputSystem
{
    public enum MouseEvent
    {
        MouseDown,
        MouseUp,
        MousePressed
    }
}