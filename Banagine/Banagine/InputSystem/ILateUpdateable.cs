﻿namespace Banagine.InputSystem
{
    public interface ILateUpdateable
    {
        void LateUpdate();
    }
}