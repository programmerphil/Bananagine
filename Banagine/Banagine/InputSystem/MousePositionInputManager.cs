﻿using System.Windows;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using Point = System.Drawing.Point;

namespace Banagine.InputSystem
{
    public class MousePositionInputManager : PullInputManager<Vector<float>>
    {
        protected override Vector<float> GetInput()
        {
            Point mousePosition = Cursor.Position;
            return Vector2Builder.Create(mousePosition.X, mousePosition.Y);
        }
    }
}