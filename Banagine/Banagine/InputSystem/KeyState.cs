﻿namespace Banagine.InputSystem
{
    public enum KeyState
    {
        Pressed,
        Down,
        Up
    }
}