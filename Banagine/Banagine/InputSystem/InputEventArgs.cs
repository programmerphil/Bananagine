﻿using System;

namespace Banagine.InputSystem
{
    public class InputEventArgs : EventArgs
    {
        public object Input { get; protected set; }

        public InputEventArgs(object input)
        {
            Input = input;
        }
    }
}