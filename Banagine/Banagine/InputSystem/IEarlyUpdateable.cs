﻿namespace Banagine.InputSystem
{
    public interface IEarlyUpdateable
    {
        void EarlyUpdate();
    }
}