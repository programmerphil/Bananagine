﻿using System.Windows.Input;

namespace Banagine.InputSystem
{
    public class PcKeyboardPullInputManager : PullInputManager<KeyInputData, Key>, ILateUpdateable, IEarlyUpdateable
    {
        private KeyboardDevice _keyboardDevice;
        private KeyboardDevice _lastKeyboardDevice;

        public PcKeyboardPullInputManager()
        {
            _keyboardDevice = Keyboard.PrimaryDevice;
            _lastKeyboardDevice = Keyboard.PrimaryDevice;
        }

        public bool KeyDown(Key key)
        {
            return _keyboardDevice.IsKeyDown(key) && !_lastKeyboardDevice.IsKeyDown(key);
        }

        public bool KeyPressed(Key key)
        {
            return _keyboardDevice.IsKeyDown(key);
        }

        public  bool KeyUp(Key key)
        {
            return !_keyboardDevice.IsKeyDown(key) && _lastKeyboardDevice.IsKeyDown(key);
        }

        public void EarlyUpdate()
        {
            _keyboardDevice = Keyboard.PrimaryDevice;
        }

        public void LateUpdate()
        {
            _lastKeyboardDevice = _keyboardDevice;
        }

        protected override KeyInputData GetInput(Key key)
        {
            KeyState keyState = KeyState.Up;

            if (KeyDown(key))
            {
                keyState = KeyState.Down;
            }
            else if (KeyPressed(key))
            {
                keyState = KeyState.Pressed;
            }
            else if (KeyUp(key))
            {
                keyState = KeyState.Up;
            }

            return new KeyInputData(keyState, key);
        }
    }
}