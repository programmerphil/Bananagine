﻿using System;

namespace Banagine.InputSystem
{
    public class OutputEventArgs<TOutput> : EventArgs
    {
        public TOutput Output;

        public OutputEventArgs(TOutput output)
        {
            Output = output;
        }
    }
}