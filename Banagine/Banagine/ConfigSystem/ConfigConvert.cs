﻿using System.IO;
using Newtonsoft.Json;

namespace Banagine.ConfigSystem
{
    public class ConfigConvert
    {
        public T DeSerialize<T>(string jsonLoadPath)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(jsonLoadPath));
        }

        public string Serialize(object value, string jsonSavePath)
        {
            string serializedObject = JsonConvert.SerializeObject(value);
            File.WriteAllText(jsonSavePath, serializedObject);
            return serializedObject;
        }
    }
}