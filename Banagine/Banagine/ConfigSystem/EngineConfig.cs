﻿using Banagine.Asset_loading_system.Loader;
using Banagine.Rendering_system.Assets;
using Banagine.SoundSystem;

namespace Banagine.ConfigSystem
{
    public class EngineConfig
    {
        public AssetLoaderConfigData<ISound> SoundAssetLoaderConfigData;
        public AssetLoaderConfigData<ITextureData> TextureAssetLoaderConfigData;
        public AssetLoaderConfigData<IShaderData> ShaderAssetLoaderConfigData;
    }
}