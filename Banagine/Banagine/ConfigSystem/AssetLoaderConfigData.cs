﻿using System;
using Banagine.Asset_loading_system;
using Banagine.Asset_loading_system.Loader;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Pipeline;

namespace Banagine.ConfigSystem
{
    public class AssetLoaderConfigData<TAsset> where TAsset : IAssetData
    {
        public AssetFilePathsConfigData AssetFilePathsConfigData;
        [JsonConverter(typeof(StringEnumConverter))]
        public AssetLoadType AssetLoadType;

        public AssetLoaderConfigData()
        {
            
        }

        public AssetLoaderConfigData(AssetFolderPath assetFolderPath, AssetLoadType assetLoadType)
        {
            AssetLoadType = assetLoadType;
            AssetFilePathsConfigData = new AssetFilePathsConfigData(assetFolderPath);
        }

        public AssetLoader<TAsset> Load(string projectPath, IAssetFactory<TAsset> assetFactory)
        {
            return new AssetLoader<TAsset>(AssetFilePathsConfigData.Load(projectPath), AssetLoadType, assetFactory);
        }
    }
}