﻿using Banagine.Asset_loading_system.Loader;
using Pipeline;

namespace Banagine.ConfigSystem
{
    public class AssetFilePathsConfigData
    {
        public string FolderName;
        public string AssetPath;
        public string[] FileExtensions;

        public AssetFilePathsConfigData()
        {
            
        }

        public AssetFilePathsConfigData(AssetFolderPath assetFolderPath)
        {
            FolderName = assetFolderPath.FolderName;
            AssetPath = assetFolderPath.AssetPath;
            FileExtensions = assetFolderPath.FileExtensions;
        }

        public AssetFolderPath Load(string projectPath)
        {
            AssetFolderPath assetFolderPath = new AssetFolderPath(projectPath, AssetPath, FolderName, FileExtensions);
            assetFolderPath.LoadFilePaths();
            return assetFolderPath;
        }
    }
}