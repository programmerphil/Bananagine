﻿using Banagine.Rendering_system;

namespace Banagine.EC
{
    public interface IRenderable
    {
        void Render(IRenderer renderer);
    }
}