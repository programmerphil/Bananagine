﻿namespace Banagine.EC
{
    public interface IUpdateable
    {
        void Update();
    }
}