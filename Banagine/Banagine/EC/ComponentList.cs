using System;
using System.Collections;
using System.Collections.Generic;

namespace Banagine.EC
{
    public class ComponentList : IEnumerable<Component>
    {
        public event Action AddedComponents;
        public event Action RemovedComponents;

        private DelayedList<Component> _components;

        public ComponentList()
        {
            _components = new DelayedList<Component>();
            _components.AllItemsAdded += OnAddedComponents;
            _components.AllItemsRemoved += OnRemovedComponents;
        }

        public void UpdateList()
        {
            _components.UpdateList();
        }

        private void OnAddedComponents(List<Component> componentsAdded)
        {
            foreach (var component in componentsAdded)
            {
                component.OnAddedToGameObject();
            }
            AddedComponents?.Invoke();
        }

        private void OnRemovedComponents(List<Component> componentsAdded)
        {
            foreach (var component in componentsAdded)
            {
                component.OnRemovedFromGameObject();
            }
            RemovedComponents?.Invoke();
        }

        public Component Add(Component component)
        {
            _components.Add(component);
            return component;
        }

        public Component RemoveComponent(Component component)
        {
            _components.Remove(component);
            component.OnRemovedFromGameObject();
            return component;
        }

        public Component RemoveComponent<T>() where T : Component
        {
            Component componentToRemove = _components.GetItem<T>();
            componentToRemove.OnRemovedFromGameObject();
            _components.Remove<T>();
            return componentToRemove;
        }

        public void RemoveComponents<T>() where T : Component
        {
            foreach (var componentToRemove in _components.GetItems<T>())
            {
                componentToRemove.OnRemovedFromGameObject();
            }
            _components.RemoveItems<T>();
        }

        public void RemoveAllComponents()
        {
            foreach (var component in _components)
            {
                component.OnRemovedFromGameObject();
                component.RemoveFromGameObject();
            }
            _components.RemoveAll();
        }

        public List<T> GetComponents<T>() where T : Component
        {
            return _components.GetItems<T>();
        }

        public T GetComponent<T>() where T : Component
        {
            return _components.GetItem<T>();
        }

        public void OnComponentEnabled()
        {
            foreach (var component in _components)
            {
                component.OnEnabled();
                
            }
        }

        public void OnComponentDisabled()
        {
            foreach (var component in _components)
            {
                component.OnDisabled();
            }
        }

        public IEnumerator<Component> GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}