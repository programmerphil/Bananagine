﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Drawing;
using Banagine.Rendering_system;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.EC
{
    public class GameObject : IRenderable, IUpdateable
    {
        public Transform Transform;

        private ComponentList _components;
        private bool _enabled;

        public string Name { get; set; }

        public string Tag { get; set; }

        public bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                if (_enabled == value) return;

                _enabled = value;
                if (!_enabled)
                {
                    _components.OnComponentDisabled();
                }
                if (_enabled)
                {
                    _components.OnComponentEnabled();
                }
            }
        }

        public GameObject(Transform transform)
        {
            Transform = transform;
            transform.GameObject = this;
            _components = new ComponentList();
        }

        public GameObject(Vector<float> position) : this(new Transform(position))
        {
            
        }

        public GameObject AddComponent(Component component)
        {
            component.Init(this);
            _components.Add(component);
            return this;
        }

        public void AddComponents(List<Component> components)
        {
            foreach (var component in components)
            {
                AddComponent(component);
            }
        }

        public T GetComponent<T>() where T : Component
        {
            return _components.GetComponent<T>();
        }

        public List<T> GetComponents<T>() where T : Component
        {
            return _components.GetComponents<T>();
        }

        public T GetComponentInChildren<T>() where T : Component
        {
            return FindComponentInChildren<T>(
                (childTransform => childTransform.GameObject.GetComponent<T>() != null));
        }

        public T GetComponentInChildrenWithTag<T>(string tag) where T : Component
        {
            return FindComponentInChildren<T>(
                  (childTransform => childTransform.GameObject.GetComponent<T>() != null &&
                  childTransform.GameObject.Tag == tag));
        }

        private T FindComponentInChildren<T>(Predicate<Transform> predicate)
           where T : Component
        {
            foreach (var child in Transform.Root.Children)
            {
                var componentInChild = FindComponentInChildren<T>(child, predicate);
                if (componentInChild != null)
                {
                    return componentInChild;
                }
            }

            return null;
        }

        private T FindComponentInChildren<T>(Transform childTranform, Predicate<Transform> predicate) where T : Component
        {
            if (predicate(childTranform))
            {
                return (T)childTranform.GameObject.GetComponent<T>();
            }

            foreach (var child in childTranform.Children)
            {
                Component childComponent = FindComponentInChildren<T>(child, predicate);
                if (childComponent != null)
                {
                    return (T)childComponent;
                }
            }

            return null;
        }

        public T GetComponentInParent<T>() where T : Component
        {
            Transform parent = Transform;

            while (parent != null)
            {
                var component = parent.GameObject.GetComponent<T>();
                if (component != null)
                {
                    return component;
                }
                parent = parent.Parent;
            }

            return null;
        }

        public T RemoveComponent<T>() where T : Component
        {
            return (T)_components.RemoveComponent<T>();
        }

        public void RemoveComponents<T>() where T : Component
        {
            _components.RemoveComponents<T>();
        }

        public Component RemoveComponent(Component component)
        {
            _components.RemoveComponent(component);
            return component;
        }

        public void RemoveAllComponents()
        {
            _components.RemoveAllComponents();
        }

        public void OnRemoved()
        {
            RemoveAllComponents();
        }

        public void Render(IRenderer renderer)
        {
            foreach (var component in _components)
            {
                IRenderable renderable = component as IRenderable;

                renderable?.Render(renderer);
            }
        }

        public void Update()
        {
            _components.UpdateList();
            foreach (var component in _components)
            {
                IUpdateable updateable = component as IUpdateable;

                updateable?.Update();
            }
        }
    }
}