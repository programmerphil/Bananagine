﻿using System;
using System.Collections.Generic;
using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.EC
{
    public class Transform
    {
        private Vector<float> _position;
        private Vector<float> _scale;
        private float _rotation;
        private List<Transform> _children;

        public Vector<float> LocalPosition { get; private set; }
        public Vector<float> LocalScale { get; private set; }

        public Vector<float> Position
        {
            get
            {
                if (HasParent())
                {
                    return Parent.Position + LocalPosition;
                }
                return  _position;
            }
            set
            {
                if (HasParent())
                {
                    LocalPosition =  value - Parent.Position;
                    _position = Vector2Builder.Create(0, 0);
                }
                else
                {
                    _position = value;
                    LocalPosition = Vector2Builder.Create(0, 0);
                }
            }
        }

        public Vector<float> Scale
        {
            get
            {
                if (HasParent())
                {
                    return _scale + LocalScale;
                }
                return _scale;
            }
            set
            {
                if (HasParent())
                {
                    LocalScale = value - Parent.Scale;
                    _scale = Vector2Builder.Create(0, 0);
                }
                else
                {
                    _scale = value;
                    LocalScale = Vector2Builder.Create(0, 0);
                }
            }
        }

        public float Rotation
        {
            get { return _rotation; }
            set
            {
                float rotationChangedAmount = value - _rotation;
                _rotation = value;

                if (_rotation > 360)
                {
                    _rotation -= 360;
                } 
                else if(_rotation <= 0)
                {
                    _rotation += 360;
                }

                if (HasParent())
                {
                    LocalPosition *= RotationMatrix.Create(rotationChangedAmount);   
                }
            }
        }

        public Transform Parent
        {
            get { return _parent; }
            set
            {
                Vector<float> positionBeforeParentChange = Position;
                Vector<float> scaleBeforeParentChange = Position;
                _parent = value;
                Position = positionBeforeParentChange;
                Scale = scaleBeforeParentChange;
            }
        }

        public GameObject GameObject;
        private Transform _parent;
        public List<Transform> Children => _children;

        public Transform Root
        {
            get
            {
                Transform root = this;

                while (root.HasParent())
                {
                    root = root.Parent;
                }

                return root;
            }
        }

        public Transform(Vector<float> position, Vector<float> scale = null, float rotation = 0)
        {
            Position = position;
            Scale = scale ?? Vector2Builder.Create(1, 1);
            Rotation = rotation;
            LocalPosition = Vector2Builder.Create(0, 0);
            LocalScale = Vector2Builder.Create(1, 1);
        }

        private bool HasParent()
        {
            return Parent != null;
        }

        public void AddChild(Transform child)
        {
            child.Parent = this;
            _children.Add(child);
        }
    }
}