﻿using Banagine.Rendering_system.Drawables;

namespace Banagine.EC.Components.Rendering
{
    public class SpriteRenderer : RenderableComponent
    {
        public Texture Texture
        {
            get { return _sprite.Texture; }
            set { _sprite.Texture = value; }
        }

        private string _textureName;
        private int _width;
        private int _height;
        private Sprite _sprite;

        public SpriteRenderer(Sprite sprite)
        {
            _sprite = sprite;
            Renderable = sprite;
        }

        public SpriteRenderer(string textureName, int width, int height)
        {
            _textureName = textureName;
            _width = width;
            _height = height;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _sprite = new Sprite(_textureName, Transform, _width, _height);
            Renderable = _sprite;
        }
    }
}