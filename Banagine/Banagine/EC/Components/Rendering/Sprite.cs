﻿using System.Drawing;
using Banagine.Rendering_system;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.OpenTk;

namespace Banagine.EC.Components.Rendering
{
    public class Sprite : IRenderable
    {
        private Transform _transform;
        private TextureLoader _textureLoader;

        public int Width { get; }
        public int Height { get; }
        public Texture Texture { get; set; }

        public Sprite(string textureName, Transform transform, int width, int height)
        {
            Width = width;
            Height = height;
            _transform = transform;
            _textureLoader = new TextureLoader();
            _textureLoader.Load(textureName, new Size(width, height), OnLoadedTexture);
        }

        private void OnLoadedTexture(OpenTkTexture texture)
        {
            Texture = texture;
        }

        public void Render(IRenderer renderer)
        {
            renderer.Render(_transform.Position, Texture);
        }
    }
}