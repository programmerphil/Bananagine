﻿using System.Drawing;
using Banagine.Rendering_system.Drawables;

namespace Banagine.EC.Components.Rendering
{
    public class ShapeRenderer : RenderableComponent
    {
        private Color _color;
        private IShape _shape;

        public ShapeRenderer(Color color, IShape shape)
        {
            _color = color;
            _shape = shape;
        }

        public ShapeRenderer(RenderShape renderShape)
        {
            Renderable = renderShape;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            Renderable = new RenderShape(Transform, _color, _shape);
        }
    }
}