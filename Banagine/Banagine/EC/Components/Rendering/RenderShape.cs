﻿using System.Drawing;
using Banagine.Assets.Shaders;
using Banagine.Asset_loading_system.Request;
using Banagine.Rendering_system;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.OpenTk;
using Banagine.Rendering_system.Shading;

namespace Banagine.EC.Components.Rendering
{
    public class RenderShape : IRenderable
    {
        private Color _color;
        private OpenTkShapeDrawable _polygon;
        private Transform _transform;
        private IShape _shape;

        public RenderShape(Transform transform, Color color, IShape shape)
        {
            _transform = transform;
            _color = color;
            _shape = shape;
            AssetRequestLoader assetRequestLoader = new AssetRequestLoader(new AssetCallInfo<ShaderData, ShaderData>(OnLoadedShaders));
            assetRequestLoader.Load(new AssetRequestData[] { new AssetRequestData<ShaderData>(Shaders.DefaultVertexPolygonShader, Shaders.DefaultFragmentShader) });
        }

        private void OnLoadedShaders(ShaderData vertexShaderData, ShaderData fragmentShaderData)
        {
            _polygon = new OpenTkShapeDrawable(_color, new[] { vertexShaderData, fragmentShaderData }, _shape);
        }

        public void Render(IRenderer renderer)
        {
            renderer.Render(_transform.Position, _polygon);
        }
    }
}