﻿using Banagine.Rendering_system;

namespace Banagine.EC.Components.Rendering
{
    public abstract class RenderableComponent : Component, IRenderable
    {
        public IRenderable Renderable { get; set; }

        public void Render(IRenderer renderer)
        {
            Renderable.Render(renderer);
        }
    }
}