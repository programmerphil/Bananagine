﻿using MathNet.Numerics.LinearAlgebra;
using OpenTK;

namespace Banagine.EC.Components.Rendering
{
    public class Camera2D : Component
    {
        public Matrix4 GetViewMatrix()
        {
            Vector<float> position = Transform.Position;
            return Matrix4.CreateTranslation(-position[0], -position[1], 0);
        }
    }
}