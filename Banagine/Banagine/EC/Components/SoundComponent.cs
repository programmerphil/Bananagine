﻿using Banagine.Asset_loading_system.Request;
using Banagine.EventSystem;
using Banagine.SoundSystem;

namespace Banagine.EC.Components
{
    public class SoundComponent : Component
    {
        private ISound _sound;
        private float _volume;
        private bool _playOnStart;

        public bool Looped { get; set; }

        public float Volume
        {
            get { return _volume; }
            set
            {
                _volume = value;
                EventManager.SendMessage(this, SoundEvent.SetSoundsVolume, new SoundVolumeEventArgs(_volume));
            }
        }

        public SoundComponent(string soundName, bool looped, float volume, bool playOnStart)
        {
            Looped = looped;
            Volume = volume;
            _playOnStart = playOnStart;
            AssetRequestLoader assetRequestLoader = new AssetRequestLoader(new AssetCallInfo<ISound>(OnLoadedSound));
            assetRequestLoader.Load(new AssetRequestData<ISound>(soundName));
        }

        private void OnLoadedSound(ISound sound)
        {
            _sound = sound;
            if (_playOnStart)
            {
                Play();
            }
        }

        public void Play()
        {
            EventManager.SendMessage(this, SoundEvent.PlaySound, new SoundEventArgs(_sound, Looped, Volume));
        }

        public void Stop()
        {
            EventManager.SendMessage(this, SoundEvent.StopSound);
        }
    }
}