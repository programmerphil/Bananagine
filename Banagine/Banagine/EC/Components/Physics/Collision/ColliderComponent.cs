﻿using System;
using System.Collections.Generic;
using Banagine.CollisionSystem;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.EC.Components.Physics.Collision
{
    public class ColliderComponent : Component, IUpdateable, ICollider
    {
        public Collider Collider { get; }

        public List<Vector<float>> Corners => Collider.Corners;
        public float Top => Collider.Top;
        public float Bottom => Collider.Bottom;
        public float Left => Collider.Left;
        public float Right => Collider.Right;
        public bool IsTrigger { get; }

        public float Friction
        {
            get { return Collider.Friction; }
            set { Collider.Friction = value; }
        }

        private bool _entered;

        public ColliderComponent(Collider collider, bool isTrigger = false)
        {
            Collider = collider;
            IsTrigger = isTrigger;
        }

        private void OnCollisionCheck(bool overlaps, ColliderComponent otherCollider)
        {
            if (overlaps)
            {
                if (!_entered)
                {
                    if (IsTrigger)
                    {
                        CallComponents<TriggerEnter>(enter => enter.OnTriggerEnter(otherCollider));
                    }
                    else
                    {
                        CallComponents<CollisionEnter>(enter => enter.OnCollisionEnter(otherCollider));
                    }
                    _entered = true;
                }
                else if (_entered)
                {
                    if (IsTrigger)
                    {
                        CallComponents<TriggerStay>(stay => stay.OnTriggerStay(otherCollider));
                    }
                    else
                    {
                        CallComponents<CollisionStay>(stay => stay.OnCollisionStay(otherCollider));
                    }
                }
            }
            else
            {
                if (_entered)
                {
                    if (IsTrigger)
                    {
                        CallComponents<OnTriggerExit>(exit => exit.OnTriggerExit(otherCollider));
                    }
                    else
                    {
                        CallComponents<CollisionExit>(exit => exit.OnCollisionExit(otherCollider));
                    }
                    _entered = false;
                }
            }
        }

        private void CallComponents<T>(Action<T> callAction) where T : class 
        {
            foreach (var component in GetComponents<Component>())
            {
                T convertedComponent = component as T;
                if (convertedComponent != null)
                {
                    callAction.Invoke(convertedComponent);
                }
            }
        }

        public void Update()
        {
            Collider.Update(Transform.Position);
        }

        public bool CollidesWith(ICollider otherCollider)
        {
            Collider.Update((otherCollider as ColliderComponent).Collider);
            return Collider.Status == ColliderStatus.Collision;
        }

        public void Update(Vector<float> position)
        {
            Collider.Update(position);
        }

        public void Update(ICollider otherCollider)
        {
            OnCollisionCheck(CollidesWith(otherCollider), otherCollider as ColliderComponent);
        }
    }
}