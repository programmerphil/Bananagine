﻿using Banagine.CollisionSystem;

namespace Banagine.EC.Components.Physics.Collision
{
    public interface CollisionEnter
    {
        void OnCollisionEnter(ColliderComponent otherCollider);
    }
}