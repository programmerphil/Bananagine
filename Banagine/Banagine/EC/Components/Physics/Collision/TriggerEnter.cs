﻿namespace Banagine.EC.Components.Physics.Collision
{
    public interface TriggerEnter
    {
        void OnTriggerEnter(ColliderComponent otherCollider);
    }
}