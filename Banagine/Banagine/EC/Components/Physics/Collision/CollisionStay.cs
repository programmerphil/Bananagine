﻿namespace Banagine.EC.Components.Physics.Collision
{
    public interface CollisionStay
    {
        void OnCollisionStay(ColliderComponent otherCollider);
    }
}