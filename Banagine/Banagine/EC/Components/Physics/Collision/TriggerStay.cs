﻿namespace Banagine.EC.Components.Physics.Collision
{
    public interface TriggerStay
    {
        void OnTriggerStay(ColliderComponent otherCollider);
    }
}