﻿namespace Banagine.EC.Components.Physics.Collision
{
    public interface OnTriggerExit
    {
        void OnTriggerExit(ColliderComponent otherCollider);
    }
}