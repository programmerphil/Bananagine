﻿namespace Banagine.EC.Components.Physics.Collision
{
    public interface CollisionExit
    {
        void OnCollisionExit(ColliderComponent otherCollider);
    }
}