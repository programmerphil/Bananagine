﻿using Banagine.EC.Components.Physics.Collision;
using Banagine.PhysicsSystem;

namespace Banagine.EC.Components.Physics
{
    public class Rigidbody2DComponent : Component, IUpdateable
    {
        private Rigidbody2D _rigidbody2D;
        private RigidbodyConfigData _rigidbodyConfigData;

        public Rigidbody2DComponent(RigidbodyConfigData rigidbodyConfigData)
        {
            _rigidbodyConfigData = rigidbodyConfigData;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _rigidbody2D = new Rigidbody2D(GetComponent<ColliderComponent>().Collider, _rigidbodyConfigData);
        }

        public void Update()
        {
            _rigidbody2D.Update();
            Transform.Position += _rigidbody2D.DeltaVelocity;
            Transform.Rotation = _rigidbody2D.DeltaRotation;
        }
    }
}