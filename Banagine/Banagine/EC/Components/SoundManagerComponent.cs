﻿using Banagine.EventSystem;
using Banagine.SoundSystem;

namespace Banagine.EC.Components
{
    public class SoundManagerComponent : Component
    {
        public void StopAll()
        {
            EventManager.SendMessage(this, SoundEvent.StopAllSounds);
        }
    }
}