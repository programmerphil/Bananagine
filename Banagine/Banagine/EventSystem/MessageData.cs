﻿using System;

namespace Banagine.EventSystem
{
    public class MessageData
    {
        public object Sender;
        public EventArgs EventArgs;
        public Enum EventType;

        public MessageData(object sender, Enum eventType, EventArgs eventArgs)
        {
            Sender = sender;
            EventType = eventType;
            EventArgs = eventArgs;
        }
    }
}