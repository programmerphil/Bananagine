﻿using System;

namespace Banagine.EventSystem
{
    public class EventDataWithEventType : EventData
    {
        private Action<object, EventArgs, Enum> _action;

        public EventDataWithEventType(Action<object, EventArgs, Enum> action, Enum eventType, int priority = 0) : base(eventType, priority)
        {
            _action = action;
        }

        public void Call(object sender, EventArgs e, Enum eventType)
        {
            _action.Invoke(sender, e, eventType);
        }
    }
}