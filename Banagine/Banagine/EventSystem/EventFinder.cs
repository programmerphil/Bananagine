﻿using System.Collections.Generic;

namespace Banagine.EventSystem
{
    public class EventFinder : IEventsFinder
    {
        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            return new List<IEventReceiver>()
            {
                (this as IEventReceiver)
            };
        }
    }
}