using Banagine.EC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Banagine.EventSystem
{
    public static class EventManager
    {
        private static DelayedList<MessageData> _messages;
        private static List<IEventReceiver> _eventReceivers;
        private static List<IEventsFinder> _eventsFinders;
        private static bool _debug;

        public static void Init(bool debug = true, params IEventsFinder[] eventsFinders)
        {
            _debug = debug;
            _messages = new DelayedList<MessageData>();
            _eventReceivers = new List<IEventReceiver>();
            _eventsFinders = eventsFinders.ToList();
        }

        public static void SendMessage(MessageData messageData)
        {
            _messages.Add(messageData);
        }

        public static void SendMessage(object sender, Enum eventType, EventArgs e, bool sendInstantly = false)
        {
            _messages.Add(new MessageData(sender, eventType, e));
            if (sendInstantly)
            {
                Update();
            }
        }

        public static void SendMessage(object sender, Enum eventType, bool sendInstantly = false)
        {
            SendMessage(sender, eventType, EventArgs.Empty, sendInstantly);
        }

        public static void AddEventReceiver(IEventReceiver eventReceiver)
        {
            _eventReceivers.Add(eventReceiver);
        }

        public static void RemoveEventReceiver(IEventReceiver eventReceiver)
        {
            _eventReceivers.Remove(eventReceiver);
        }

        public static void Update()
        {
            _messages.UpdateList();
            List<IEventReceiver> eventReceivers = new List<IEventReceiver>();
            eventReceivers.AddRange(_eventReceivers);

            foreach (var eventsFinder in _eventsFinders)
            {
                eventReceivers.AddRange(eventsFinder.GetEventReceivers());
            }

            foreach (var message in _messages)
            {
                foreach (var eventReceiver in eventReceivers)
                {
                    foreach (EventData eventData in eventReceiver.EventDatas)
                    {
                        if (Equals(message.EventType, eventData.EventType) || Equals(eventData.EventType, EngineEvent.All))
                        {
                            Type eventDataType = eventData.GetType();

                            MethodInfo callMethodInfo = eventDataType.GetMethod("Call");

                            ParameterInfo[] callParameters = callMethodInfo.GetParameters();

                            bool isCorrectParameterType = callParameters[1].ParameterType == message.EventArgs.GetType();

                            if (callParameters.Length == 2 && isCorrectParameterType)
                            {
                                eventDataType.GetMethod("Call").Invoke(eventData, new[] { message.Sender, message.EventArgs });
                            }

                            if (callParameters.Length == 3 && isCorrectParameterType)
                            {
                                eventDataType.GetMethod("Call").Invoke(eventData, new[] { message.Sender, message.EventArgs, message.EventType });
                            }

                            if (_debug && !isCorrectParameterType)
                            {
                                System.Console.WriteLine("Wrong event args parameter type for " + eventReceiver.GetType() + " Expected " + callParameters[1] + " Actual " + message.EventArgs.GetType());
                            }
                        }
                    }
                }
            }

            _messages.RemoveAll();
        }
    }
}