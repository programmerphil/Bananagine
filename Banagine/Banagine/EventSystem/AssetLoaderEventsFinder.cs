﻿using System.Collections.Generic;
using System.Linq;
using Banagine.Asset_loading_system;

namespace Banagine.EventSystem
{
    public class AssetLoaderEventsFinder : IEventsFinder
    {
        private IEventReceiver[] _assetLoadersWithEvent;

        public AssetLoaderEventsFinder(params IEventReceiver[] assetLoadersWithEvent)
        {
            _assetLoadersWithEvent = assetLoadersWithEvent;
        }

        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            return _assetLoadersWithEvent;
        }
    }
}