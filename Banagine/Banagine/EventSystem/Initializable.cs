﻿using System;
using System.Collections.Generic;

namespace Banagine.EventSystem
{
    public abstract class Initializable : IEventReceiver
    {
        public virtual IEnumerable<EventData> EventDatas => new[] { new EventData<EventArgs>(Init, EngineEvent.Init) };

        protected abstract void Init(object sender, EventArgs e);
    }
}