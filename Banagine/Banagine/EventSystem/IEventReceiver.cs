using System;
using System.Collections.Generic;

namespace Banagine.EventSystem
{
	public interface IEventReceiver
	{
		IEnumerable<EventData> EventDatas { get; }
	}
}
