﻿using System;
using System.Collections.Generic;
using Banagine.Console;

namespace Banagine.EventSystem
{
    public class TestEventsFinder : IEventsFinder
    {
        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            EventConsole eventConsole = new EventConsole();

            return new[] { eventConsole };
        }
    }
}