namespace Banagine.EventSystem
{
	public enum EngineEvent
	{
        EarlyUpdate,
        LateUpdate,
		Update,
        UpdateCollisionManager,
		Render,
        Init,
        LoadAssets,
        GetInput,
        InputChecked,
        LoadAsset,
        LoadScene,
        AssetLoaded,
        All,
	}
}
