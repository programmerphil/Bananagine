using System;

namespace Banagine.EventSystem
{
    public class EventData
    {
        public Enum EventType;
        public int Priority;

        public EventData(Enum eventType, int priority = 0)
        {
            EventType = eventType;
            Priority = priority;
        }
    }

	public class EventData<T> : EventData where T : EventArgs
	{
		public EventMethod<T> Action;

	    public EventData(EventMethod<T> action, Enum eventType, int priority = 0) : base(eventType, priority)
	    {
	        Action = action;
	        EventType = eventType;
	        Priority = priority;
	    }

	    public void Call(object sender, T e)
	    {
	        Action.Invoke(sender, e);
	    }
	}
}
