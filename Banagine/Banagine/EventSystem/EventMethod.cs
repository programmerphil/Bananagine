using System;

namespace Banagine.EventSystem
{
	public delegate void EventMethod(object sender, EventArgs e);
    public delegate void EventMethod<T>(object sender, T e) where T : EventArgs;
}
