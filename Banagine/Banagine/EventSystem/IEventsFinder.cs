﻿using System.Collections.Generic;

namespace Banagine.EventSystem
{
    public interface IEventsFinder
    {
        IEnumerable<IEventReceiver> GetEventReceivers();
    }
}