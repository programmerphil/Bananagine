using System;
using System.Collections.Generic;

namespace Banagine.EventSystem
{
	public abstract class Updateable : IEventReceiver, IEventsFinder
	{
		public IEnumerable<EventData> EventDatas => new[] { new EventData<EventArgs>(Update, EngineEvent.Update) };

	    public abstract void Update(object sender, EventArgs e);

        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            return new IEventReceiver[] { this };
        }
    }
}
