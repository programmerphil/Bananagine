using System;
using System.Collections.Generic;
using Banagine.EventSystem;

namespace Banagine.CollisionSystem
{
	public class CollisionManager
	{
	    private ICollisionMap _collisionMap;

	    public CollisionManager(ICollisionMap collisionMap)
	    {
	        _collisionMap = collisionMap;
	    }

        public void Update(List<ICollider> colliders )
	    {
            _collisionMap.Update(colliders);

            foreach (var collider in colliders)
            {
                foreach (var colliderWithPossibleCollision  in _collisionMap.GetCollidersPossibleForCollision(collider))
                {
                    collider.Update(colliderWithPossibleCollision);
                }
            }
	    }
	}
}
