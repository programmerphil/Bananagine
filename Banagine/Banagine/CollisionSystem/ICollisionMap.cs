using System;
using System.Collections.Generic;

namespace Banagine.CollisionSystem
{
	public interface ICollisionMap
	{
		int PixelHeight { get; }

		int PixelWidth { get; }

		List<ICollider> GetCollidersPossibleForCollision(ICollider collider);

		void Update(List<ICollider> colliders);
	}
}
