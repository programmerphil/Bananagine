using System;
using System.Collections.Generic;
using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
	public class PolygonCollider : Collider<PolygonCollider, BoxCollider, CircleCollider>
	{
	    public Polygon Polygon { get; }
	    protected override IShape Shape => Polygon;

	    public override List<Vector<float>> Corners => Polygon.Corners;

	    public PolygonCollider(List<Vector<float>> corners)
	    {
	        Polygon = new Polygon(corners.ToArray());
	    }

        protected override bool CollidesWith(PolygonCollider polygonCollider)
		{
		    return Polygon.OverlapsWith(polygonCollider.Polygon);
		}

	    protected override bool CollidesWith(BoxCollider collider)
	    {
	        return Polygon.OverlapsWith(collider.Quad);
	    }

	    protected override bool CollidesWith(CircleCollider collider)
	    {
	        return Polygon.OverlapsWith(collider.Circle);
	    }
	}
}
