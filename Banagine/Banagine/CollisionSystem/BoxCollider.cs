using System;
using System.Collections.Generic;
using System.Drawing;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
	public class BoxCollider : Collider<PolygonCollider, CircleCollider, BoxCollider>
	{
	    public override List<Vector<float>> Corners => Quad.Corners;
	    protected override IShape Shape => Quad;

	    public Quad Quad { get; }
        public override float Bottom => _position.GetY() + Quad.Height;
        public override float Right => _position.GetX() + Quad.Width;

        public BoxCollider(Quad quad)
	    {
	        Quad = quad;
	    }

	    public BoxCollider(Vector<float> position, Size size) : this(new Quad(position.GetX(), position.GetX() + size.Width, position.GetY(), position.GetY() + size.Height))
	    {
	        
	    }

	    public BoxCollider(float left, float right, float top, float bottom) : this(new Quad(left, right, top, bottom))
	    {
	        
	    }

	    protected override bool CollidesWith(PolygonCollider polygonCollider)
		{
            return Quad.OverlapsWith(polygonCollider.Polygon);
        }

	    protected override bool CollidesWith(CircleCollider circleCollider)
	    {
	        return Quad.OverlapsWith(circleCollider.Circle);
	    }

	    protected override bool CollidesWith(BoxCollider boxCollider)
	    {
	        return Quad.OverlapsWith(boxCollider.Quad);
	    }
	}
}
