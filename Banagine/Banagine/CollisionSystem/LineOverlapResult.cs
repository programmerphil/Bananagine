﻿using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
    public struct LineOverlapResult
    {
        public bool Overlaps;
        public Vector<float> OverlapPoint;

        public LineOverlapResult(bool overlaps, Vector<float> overlapPoint)
        {
            Overlaps = overlaps;
            OverlapPoint = overlapPoint;
        }
    }
}