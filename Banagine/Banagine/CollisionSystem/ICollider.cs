using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
	public interface ICollider
	{
        List<Vector<float>> Corners { get; }
	    float Top { get; }
	    float Bottom { get; }
        float Left { get; }
        float Right { get; }
        float Friction { get; set; }

        bool CollidesWith(ICollider otherCollider);
	    void Update(Vector<float> position);
	    void Update(ICollider otherCollider);
	}
}
