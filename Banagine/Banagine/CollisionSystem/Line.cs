﻿using System;
using System.Collections.Generic;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Drawables;
using IrrKlang;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
    public class Line
    {
        public Vector<float> Start { get; }
        public Vector<float> End { get; }

        public float A { get; }
        public float B { get; }
        public float C { get; }

        public Line(Vector<float> start, Vector<float> end)
        {
            Start = start;
            End = end;
            A = End.GetY() - Start.GetY();
            B = Start.GetX() - End.GetX();
            C = A * Start.GetX() + B * Start.GetY();
        }

        // See here https://www.topcoder.com/community/data-science/data-science-tutorials/geometry-concepts-line-intersection-and-its-applications/ for the algorithm.
        public LineOverlapResult FindOverlapPoint(Line otherLine)
        {
            return LineSegementsIntersect(Start, End, otherLine.Start, otherLine.End);
        }

        /// <summary>
        /// Test whether two line segments intersect. If so, calculate the intersection point.
        /// <see cref="http://stackoverflow.com/a/14143738/292237"/>
        /// </summary>
        /// <param name="p">Vector to the start point of p.</param>
        /// <param name="p2">Vector to the end point of p.</param>
        /// <param name="q">Vector to the start point of q.</param>
        /// <param name="q2">Vector to the end point of q.</param>
        /// <param name="intersection">The point of intersection, if any.</param>
        /// <param name="considerOverlapAsIntersect">Do we consider overlapping lines as intersecting?
        /// </param>
        /// <returns>True if an intersection point was found.</returns>
        public  LineOverlapResult LineSegementsIntersect(Vector<float> p, Vector<float> p2, Vector<float> q, Vector<float> q2)
        {
            Vector<float> intersection = Vector2Builder.Create(0, 0);

            var r = p2 - p;
            var s = q2 - q;
            var rxs = r.Cross(s);
            var qpxr = (q - p).Cross(r);

            // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
            if (rxs.IsZero() && qpxr.IsZero())
            {
              

                // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
                // then the two lines are collinear but disjoint.
                // No need to implement this expression, as it follows from the expression above.
                return new LineOverlapResult(false, intersection);
            }

            // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
            if (rxs.IsZero() && !qpxr.IsZero())
                return new LineOverlapResult(false, intersection);

            // t = (q - p) x s / (r x s)
            var t = (q - p).Cross(s) / rxs;

            // u = (q - p) x r / (r x s)

            var u = (q - p).Cross((r)) / rxs;

            // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
            // the two line segments meet at the point p + t r = q + u s.
            if (!rxs.IsZero() && (0 <= t && t <= 1) && (0 <= u && u <= 1))
            {
                // We can calculate the intersection point using either t or u.
                intersection = p + t * r;

                // An intersection was found.
                return new LineOverlapResult(true, intersection);
            }

            // 5. Otherwise, the two line segments are not parallel but do not intersect.
           return new LineOverlapResult(false, intersection);
        }



        private bool IsBetween(float value1, float value2, float valueToCheck)
        {
            return Math.Min(value1, value2) <= valueToCheck && Math.Max(value1, value2) <= valueToCheck;
        }

        private float CalculateDeterminant(Line otherLine)
        {
            return A * otherLine.B - otherLine.A * B;
        }
    }
}