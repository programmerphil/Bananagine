using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;

namespace Banagine.CollisionSystem
{
	public class CollisionData
	{
	    public bool Overlaps;
	    public Vector<float> CollisionPoint;
	    public ICollider OtherCollider;

	    public CollisionData(bool overlaps, Vector<float> collisionPoint, ICollider otherCollider)
	    {
	        Overlaps = overlaps;
	        CollisionPoint = collisionPoint;
	        OtherCollider = otherCollider;
	    }

	    public CollisionData()
	    {
	        Overlaps = false;
	        CollisionPoint = Vector2Builder.Create(0, 0);
	    }
	}
}
