using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Banagine.CollisionSystem
{
    public class SpatialHashMap : ICollisionMap
    {
        private Dictionary<int, List<ICollider>> _map;
        private int _cols;
        private int _rows;
        private Size _cellSize;

        public int PixelHeight { get; }
        public int PixelWidth { get; }

        public SpatialHashMap(int pixelWidth, int pixelHeight, Size cellSize)
        {
            _map = new Dictionary<int, List<ICollider>>();
            PixelHeight = pixelHeight;
            PixelWidth = pixelWidth;
            _cellSize = cellSize;
            _cols = PixelHeight / _cellSize.Height;
            _rows = PixelWidth / _cellSize.Width;
            ClearMap();
        }

        public List<ICollider> GetCollidersPossibleForCollision(ICollider collider)
        {
            List<ICollider> collidersPossibleForCollision = new List<ICollider>();

            foreach (var colliderPoint in collider.Corners)
            {
                int hashId = GetHashId(colliderPoint);

                if (_map.ContainsKey(hashId))
                {
                    AddRange(collidersPossibleForCollision, _map[hashId], collider);
                }
            }

            return collidersPossibleForCollision;
        }

        private void AddRange(List<ICollider> list, IEnumerable<ICollider> possibleColliders, ICollider colliderCheckingCollisionWith)
        {
            foreach (var allowableCollider in GetAllowableColliders(possibleColliders, colliderCheckingCollisionWith))
            {
                if (!list.Contains(allowableCollider))
                {
                    list.Add(allowableCollider);
                }
            }
        }

        private List<ICollider> GetAllowableColliders(IEnumerable<ICollider> possibleColliders, ICollider colliderCheckingCollisionWith)
        {
            List<ICollider> allowableColliders = new List<ICollider>();

            foreach (var collider in possibleColliders)
            {
                if (!collider.Equals(colliderCheckingCollisionWith) && !allowableColliders.Contains(collider))
                {
                    allowableColliders.Add(collider);
                }
            }

            return allowableColliders;
        }

        public void Update(List<ICollider> colliders)
        {
            ClearMap();
            foreach (var collider in colliders)
            {
                foreach (var colliderPoint in collider.Corners)
                {
                    AddCollider(collider, colliderPoint);
                }
            }
        }

        private void ClearMap()
        {
            _map.Clear();
            for (int col = 0; col < _cols; col++)
            {
                for (int row = 0; row < _rows; row++)
                {
                    _map.Add((col * _cols) + row, new List<ICollider>());
                }
            }
        }

        private void AddCollider(ICollider collider, Vector<float> position)
        {
            int hashId = GetHashId(position);
            if (_map.ContainsKey(hashId))
            {
                List<ICollider> collidersAtId = _map[hashId];
                if (!collidersAtId.Contains(collider))
                {
                    collidersAtId.Add(collider);
                }
            }
        }

        private int GetHashId(Vector<float> position)
        {
            return
                 (int)((Math.Floor(position.GetX() / _cellSize.Width)) +
                ((Math.Floor(position.GetY() / _cellSize.Height))) * _cols);
        }
    }
}