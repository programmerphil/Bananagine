﻿using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
    public class CollisionPoint
    {
        public Vector<float> Find(List<Vector<float>> cornersFromShape1, List<Vector<float>> cornersFromShape2)
        {
            for (int i = 0; i < cornersFromShape1.Count; i++)
            {
                Line shape1Line = new Line(cornersFromShape1[i], GetCorner(i + 1, cornersFromShape1));

                for (int j = 0; j < cornersFromShape2.Count; j++)
                {
                    Line shape2Line = new Line(cornersFromShape2[j], GetCorner(j + 1, cornersFromShape2));
                    LineOverlapResult lineOverlapResult = shape1Line.FindOverlapPoint(shape2Line);
                    if (lineOverlapResult.Overlaps)
                    {
                        return lineOverlapResult.OverlapPoint;
                    }
                }
            }

            return Vector2Builder.Create(0, 0);
        }

        private Vector<float> GetCorner(int index, List<Vector<float>> corners)
        {
            if (index >= corners.Count)
            {
                return corners[0];
            }
            return corners[index];
        }
    }
}