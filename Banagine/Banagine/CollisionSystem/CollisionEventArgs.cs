using System;
using System.Collections.Generic;

namespace Banagine.CollisionSystem
{
	public class CollisionEventArgs
	{
		public CollisionData CollisionData;

	    public CollisionEventArgs(CollisionData collisionData)
	    {
	        CollisionData = collisionData;
	    }
	}
}
