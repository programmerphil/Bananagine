using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;
using MoreLinq;

namespace Banagine.CollisionSystem
{
	public abstract class Collider : ICollider
	{
	    private const string CollidesWithMethodName = "CollidesWith";

        private List<CollisionData> _collidersColldingWith;
	    private ColliderStatus _status;
	    private MethodCaller _methodCaller;

	    protected Vector<float> _position;
	    private CollisionData _currentCollisionData;
	    private CollisionPoint _collisionPoint;

        public event EventHandler<CollisionEventArgs> CollisionStatusChange;

        public abstract List<Vector<float>> Corners { get; }

	    public virtual float Top => _position.GetY();
        public virtual float Bottom => _position.GetY() + (Corners.Max(item => item.GetY()) - Corners.Min(item => item.GetY())); 
        public virtual float Left => _position.GetX();
        public virtual float Right =>  _position.GetX() - (Corners.Max(item => item.GetX()) - Corners.Min(item => item.GetX()));
        public float Friction { get; set; }

        public ColliderStatus Status
	    {
	        get { return _status; }
	        protected set
	        {
	            if (_status != value)
	            {
	                CollisionStatusChange?.Invoke(this, new CollisionEventArgs(_currentCollisionData));
	            }
                _status = value;
	        }
	    }

	    protected abstract IShape Shape { get; }

	    protected Collider(float friction = 1)
	    {
            Status = ColliderStatus.NoCollision;
            _collisionPoint = new CollisionPoint();
            _methodCaller = new MethodCaller();
            Friction = friction;
	    }

	    public void Update(ICollider otherCollider)
	    {
            _currentCollisionData = new CollisionData(CollidesWith(otherCollider), _collisionPoint.Find(Corners, otherCollider.Corners), otherCollider);
            Status = _currentCollisionData.Overlaps ? ColliderStatus.Collision : ColliderStatus.NoCollision;
	    }

	    public void Update(Vector<float> position)
	    {
	        _position = position;
	        Shape.Position = position;
	    }

	    protected bool CallCollidesWithMethod(params object[] parameters)
	    {
	        return _methodCaller.Call<bool>(this, CollidesWithMethodName, BindingFlags.Instance | BindingFlags.NonPublic,
	            parameters);
	    }

	    public abstract bool CollidesWith(ICollider otherCollider);
	}

    public abstract class Collider<TCollideableCollider1> : Collider
    {
        public override bool CollidesWith(ICollider otherCollider)
        {
            return CallCollidesWithMethod(otherCollider);
        }

        protected abstract bool CollidesWith(TCollideableCollider1 otherCollider);
    }

    public abstract class Collider<TCollideableCollider1, TCollideableCollider2> : Collider
    {
        public override bool CollidesWith(ICollider otherCollider)
        {
            return CallCollidesWithMethod(otherCollider);
        }

        protected abstract bool CollidesWith(TCollideableCollider1 otherCollider);
        protected abstract bool CollidesWith(TCollideableCollider2 otherCollider);
    }

    public abstract class Collider<TCollideableCollider1, TCollideableCollider2, TCollideableCollider3> : Collider
    {
        public override bool CollidesWith(ICollider otherCollider)
        {
            return CallCollidesWithMethod(otherCollider);
        }

        protected abstract bool CollidesWith(TCollideableCollider1 otherCollider);
        protected abstract bool CollidesWith(TCollideableCollider2 otherCollider);
        protected abstract bool CollidesWith(TCollideableCollider3 otherCollider);
    }

    public abstract class Collider<TCollideableCollider1, TCollideableCollider2, TCollideableCollider3, TCollideableCollider4> : Collider
    {
        public override bool CollidesWith(ICollider otherCollider)
        {
            return CallCollidesWithMethod(otherCollider);
        }

        protected abstract bool CollidesWith(TCollideableCollider1 otherCollider);
        protected abstract bool CollidesWith(TCollideableCollider2 otherCollider);
        protected abstract bool CollidesWith(TCollideableCollider3 otherCollider);
        protected abstract bool CollidesWith(TCollideableCollider4 otherCollider);
    }
}
