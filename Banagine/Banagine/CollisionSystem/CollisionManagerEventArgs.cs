﻿using System;
using System.Collections.Generic;
using Banagine.EventSystem;

namespace Banagine.CollisionSystem
{
    public class CollisionManagerEventArgs : EventArgs
    {
        public List<ICollider> Colliders;

        public CollisionManagerEventArgs(List<ICollider> colliders)
        {
            Colliders = colliders;
        }
    }
}