using System;
using System.Collections.Generic;

namespace Banagine.CollisionSystem
{
	public enum ColliderStatus
	{
		Collision,
		NoCollision
	}
}
