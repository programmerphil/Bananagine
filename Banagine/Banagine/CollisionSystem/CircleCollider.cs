using System;
using System.Collections.Generic;
using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.CollisionSystem
{
	public class CircleCollider : Collider<BoxCollider, PolygonCollider, CircleCollider>
	{
	    public Circle Circle { get; }

	    protected override IShape Shape => Circle;

	    public CircleCollider(Circle circle)
	    {
	        Circle = circle;
	    }

	    public CircleCollider(float radius, Vector<float> center) : this(new Circle(radius, center))
	    {
	        
	    }

	    public override List<Vector<float>> Corners => Circle.Corners;

        protected override bool CollidesWith(BoxCollider collider)
        {
            return Circle.OverlapsWith(collider.Quad);
        }

	    protected override bool CollidesWith(PolygonCollider collider)
	    {
	        return Circle.OverlapsWith(collider.Polygon);
	    }

	    protected override bool CollidesWith(CircleCollider collider)
	    {
	        return Circle.OverlapsWith(collider.Circle);
	    }
	}
}
