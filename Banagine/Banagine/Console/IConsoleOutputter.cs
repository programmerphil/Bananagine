﻿namespace Banagine.Console
{
    public interface IConsoleOutputter
    {
        void Output(string text);
    }
}