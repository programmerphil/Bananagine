﻿namespace Banagine.Console
{
    public class StandardConsoleOutputter : IConsoleOutputter
    {
        public void Output(string text)
        {
            System.Console.WriteLine(text);
        }
    }
}