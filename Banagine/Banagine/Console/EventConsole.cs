﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Banagine.EventSystem;

namespace Banagine.Console
{
    public class EventConsole : IEventReceiver
    {
        public IEnumerable<EventData> EventDatas => new[] { new EventDataWithEventType(OnEvent, EngineEvent.All),  };

        private IConsoleOutputter _consoleOutputter;

        public EventConsole()
        {
            _consoleOutputter = new StandardConsoleOutputter();
        }

        public void OnEvent(object sender, EventArgs e, Enum eventType)
        {
            string eventString = "Event type is " + eventType + " " + Environment.NewLine;
            eventString += "Sender was " + sender + " " + Environment.NewLine;
            Type eventArgsType = e.GetType();

            eventString += GetMemberInfoDescriptions(eventArgsType.GetFields(), e);
            eventString += GetMemberInfoDescriptions(eventArgsType.GetProperties(), e);

            _consoleOutputter.Output(eventString);
        }

        private string GetMemberInfoDescriptions(IEnumerable<MemberInfo> memberInfos, object membersObject)
        {
            string valuesAsString = "";

            foreach (var memberInfo in memberInfos)
            {
                valuesAsString += "Member name " + memberInfo.Name + " Value : " +
                                  GetValueAsString(memberInfo, membersObject)
                                  + Environment.NewLine;
            }

            return valuesAsString;
        }

        private string GetValueAsString(MemberInfo memberInfo, object membersObject)
        {
            string valueAsString = "";

            FieldInfo fieldInfo = memberInfo as FieldInfo;
            
            if (fieldInfo != null)
            {
                valueAsString = fieldInfo.GetValue(membersObject).ToString();
            }

            PropertyInfo propertyInfo = memberInfo as PropertyInfo;
            if (propertyInfo != null)
            {
                valueAsString = propertyInfo.GetValue(membersObject, null).ToString();
            }

            return valueAsString;
        }
    }
}