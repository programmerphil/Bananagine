﻿using System;

namespace Banagine.Asset_loading_system.Args
{
    public class AssetLoadingEventArgs : EventArgs
    {
        public string AssetName;
        public bool UseGenericAssetLoadedMessage;
        public Type AssetType;

        public AssetLoadingEventArgs(string assetName, Type assetType, bool useGenericAssetLoadedMessage = true)
        {
            AssetType = assetType;
            AssetName = assetName;
            UseGenericAssetLoadedMessage = useGenericAssetLoadedMessage;
        }
    }

    public class AssetLoadingEventArgs<TAsset> : EventArgs
    {
        public string AssetName;
        public bool UseGenericAssetLoadedMessage;

        public AssetLoadingEventArgs(string assetName, bool useGenericAssetLoadedMessage = true)
        {
            AssetName = assetName;
            UseGenericAssetLoadedMessage = useGenericAssetLoadedMessage;
        }
    }
}