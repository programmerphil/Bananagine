﻿using System;

namespace Banagine.Asset_loading_system.Args
{
    public class AssetLoadedEventArgs : EventArgs
    {
        public IAssetData AssetData;

        public AssetLoadedEventArgs(IAssetData assetData)
        {
            AssetData = assetData;
        }
    }

    public class AssetLoadedEventArgs<TAsset> : EventArgs
    {
        public TAsset Asset;

        public AssetLoadedEventArgs(TAsset asset)
        {
            Asset = asset;
        }
    }
}