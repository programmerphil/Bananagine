﻿namespace Banagine.Asset_loading_system.Loader
{
    public interface IAssetFactory<TAsset> where TAsset : IAssetData
    {
        TAsset CreateAssetData(string fileName, string filePath, RenderingSystem renderingSystem);
    }
}