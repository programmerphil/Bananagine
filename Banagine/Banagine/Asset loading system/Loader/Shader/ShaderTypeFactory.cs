﻿using OpenTK.Graphics.OpenGL;

namespace Banagine.Asset_loading_system.Loader.Shader
{
    public class ShaderTypeFactory
    {
        public ShaderType GetShaderType(string fileExtension)
        {
            switch (fileExtension)
            {
                case "vertexShader":
                    return ShaderType.VertexShader;
                
                case "fragmentShader":
                    return ShaderType.FragmentShader;

                default:
                    return ShaderType.ComputeShader;
            }
        }
    }
}