﻿using System;
using System.IO;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Assets;
using Banagine.Rendering_system.Shading;

namespace Banagine.Asset_loading_system.Loader.Shader
{
    public class ShaderFactory : IAssetFactory<IShaderData>
    {
        private ShaderTypeFactory _shaderTypeFactory;

        public ShaderFactory()
        {
            _shaderTypeFactory = new ShaderTypeFactory();
        }

        public IShaderData CreateAssetData(string fileName, string filePath, RenderingSystem renderingSystem)
        {
            string content = "";

            switch (renderingSystem)
            {
                case RenderingSystem.OpenTk:
                    using (StreamReader streamReader = new StreamReader(filePath))
                    {
                        content = streamReader.ReadToEnd();
                    }

                    return new ShaderData(fileName, content, _shaderTypeFactory.GetShaderType(filePath.GetFileExtension()));
                default:
                    throw new ArgumentOutOfRangeException(nameof(renderingSystem), renderingSystem, null);
            }
        }
    }
}