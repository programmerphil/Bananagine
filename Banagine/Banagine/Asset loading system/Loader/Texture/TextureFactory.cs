﻿using System;
using System.Drawing;
using Banagine.Rendering_system.Assets;
using Banagine.Rendering_system.OpenTk;

namespace Banagine.Asset_loading_system.Loader.Texture
{
    public class TextureFactory : IAssetFactory<ITextureData>
    {
        public ITextureData CreateAssetData(string fileName, string filePath, RenderingSystem renderingSystem)
        {
            switch (renderingSystem)
            {
                case RenderingSystem.OpenTk:
                    Bitmap image = new Bitmap(filePath);
                    return new OpenTkTextureData(fileName, image);
                default:
                    throw new ArgumentOutOfRangeException(nameof(renderingSystem), renderingSystem, null);
            }
        }
    }
}