﻿using System;
using System.Collections.Generic;
using Banagine.Asset_loading_system.Args;
using Banagine.EventSystem;
using Banagine.ExtensionsMethods;
using Pipeline;

namespace Banagine.Asset_loading_system.Loader
{
    public class AssetLoader<TAsset> : IAssetLoader<TAsset>, IEventReceiver where TAsset : IAssetData
    {
        public  IEnumerable<EventData> EventDatas => new List<EventData>()
                {
                    new EventData<EventArgs>(LoadAssets, EngineEvent.LoadAssets),
                    new EventData<AssetLoadingEventArgs<TAsset>>(LoadAsset, EngineEvent.LoadAsset),
                    new EventData<AssetLoadingEventArgs>(LoadAsset, EngineEvent.LoadAsset)
                };

        protected AssetFolderPath AssetFolderPathToLoad;
        protected Dictionary<string, TAsset> _assets;

        private AssetLoadType _assetLoadType;
        private IAssetFactory<TAsset> _assetFactory;

        public AssetLoader(AssetFolderPath assetFolderPathToLoad, AssetLoadType assetLoadType, IAssetFactory<TAsset> assetFactory)
        {
            AssetFolderPathToLoad = assetFolderPathToLoad;
            _assetLoadType = assetLoadType;
            _assetFactory = assetFactory;
            _assets = new Dictionary<string, TAsset>();
        }

        protected void LoadAssets(object sender, EventArgs e)
        {
            if (_assetLoadType == AssetLoadType.PreLoaded)
            {
                LoadAll();
            }
        }

        protected void LoadAsset(object sender, AssetLoadingEventArgs e)
        {
            if (e.AssetType != typeof(TAsset) && !typeof(TAsset).IsAssignableFrom(e.AssetType)) return;

            string assetName = e.AssetName;
            TAsset asset = default(TAsset);

            switch (_assetLoadType)
            {
                case AssetLoadType.PreLoaded:
                    if (_assets.ContainsKey(assetName))
                    {
                        asset = _assets[assetName];
                    }
                    break;
                case AssetLoadType.OnDemand:
                    asset = LoadViaFileName(assetName);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            if (e.UseGenericAssetLoadedMessage)
            {
                EventManager.SendMessage(this, EngineEvent.AssetLoaded, new AssetLoadedEventArgs<TAsset>(asset));
            }
            else
            {
                EventManager.SendMessage(this, EngineEvent.AssetLoaded, new AssetLoadedEventArgs(asset));
            }
        }

        protected void LoadAsset(object sender, AssetLoadingEventArgs<TAsset> e)
        {
            LoadAsset(sender, new AssetLoadingEventArgs(e.AssetName, typeof(TAsset), e.UseGenericAssetLoadedMessage));
        }

        public Dictionary<string, TAsset> LoadAll()
        {
            return LoadAssets();
        }

        private Dictionary<string, TAsset> LoadAssets()
        {
            foreach (var filePath in AssetFolderPathToLoad.FilePaths)
            {
                string fileName = filePath.GetFileName();

                _assets.Add(fileName, Load(filePath));
            }

            return _assets;
        }

        public TAsset Load(string filePath)
        {
            return _assetFactory.CreateAssetData(filePath.GetFileName(), filePath, RenderingSystem.OpenTk);
        }

        public TAsset LoadViaFileName(string fileName)
        {
            string filePath = AssetFolderPathToLoad.FilePaths.Find(item => item.GetFileName().Equals(fileName, StringComparison.CurrentCultureIgnoreCase));

            if (!string.IsNullOrEmpty(filePath))
            {
                return Load(filePath);
            }

            return default(TAsset);
        }
    }
}