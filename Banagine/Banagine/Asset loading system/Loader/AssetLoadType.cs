﻿namespace Banagine.Asset_loading_system.Loader
{
    public enum AssetLoadType
    {
        PreLoaded,
        OnDemand
    }
}