﻿using System.Collections.Generic;

namespace Banagine.Asset_loading_system.Loader
{
    public interface IAssetLoader<TAsset>  where TAsset : IAssetData
    {
        Dictionary<string, TAsset> LoadAll();
        TAsset Load(string filePath);
    }
}