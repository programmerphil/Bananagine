﻿using System;
using Banagine.SoundSystem;

namespace Banagine.Asset_loading_system.Loader.Sound
{
    public class SoundFactory : IAssetFactory<ISound>
    {
        public ISound CreateAssetData(string fileName, string filePath, RenderingSystem renderingSystem)
        {
            return new IrrKlangSound(filePath);
        }
    }
}