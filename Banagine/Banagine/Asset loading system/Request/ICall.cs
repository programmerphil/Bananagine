namespace Banagine.Asset_loading_system.Request
{
    public interface ICall
    {
        void Call(object[] parameters);
    }
}