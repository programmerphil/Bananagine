﻿using System;
using System.Collections.Generic;

namespace Banagine.Asset_loading_system.Request
{
    public class AssetRequestResult
    {
        private List<IAssetData> _assets;

        public AssetRequestResult(List<IAssetData> assets)
        {
            _assets = assets;
        }

        public T GetAsset<T>(string assetName) where T : IAssetData
        {
            IAssetData assetData = _assets.Find(item => item.Name == assetName);
            if (assetData == null)
            {
                throw new Exception("AssetData dosn't exists");
            }
            if (assetData.GetType() != typeof(T))
            {
                throw new Exception("An asset with the name exist, but not of the type of asset you want to load." +
                                " \n You wanted to load" + typeof(T).Name + " but the only type that exists is " + assetData.GetType().Name);
            }

            return (T)assetData;
        }

        public List<T> GetAll<T>() where T : IAssetData
        {
            List<T> assetsMatchingType = new List<T>();

            foreach (var assetData in _assets)
            {
                if (assetData.GetType() == typeof(T))
                {
                    assetsMatchingType.Add((T)assetData);
                }
            }

            return assetsMatchingType;
        }
    }
}