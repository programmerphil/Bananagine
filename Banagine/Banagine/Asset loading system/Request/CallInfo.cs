using System;
using System.Reflection;

namespace Banagine.Asset_loading_system.Request
{
    public abstract class CallInfo : ICall
    {
        private MethodInfo _method;
        private object _target;

        protected CallInfo(MethodInfo method, object target)
        {
            _method = method;
            _target = target;
        }

        public void Call(object[] parameters)
        {
            _method.Invoke(_target, parameters);
        }
    }

    public class AssetCallInfo<T1> : CallInfo
    {
        public AssetCallInfo(Action<T1> action) : base(action.Method, action.Target)
        {
        }
    }

    public class AssetCallInfo<T1, T2> : CallInfo
    {
        public AssetCallInfo(Action<T1, T2> action) : base(action.Method, action.Target)
        {
        }
    }

    public class AssetCallInfo<T1, T2, T3> : CallInfo
    {
        public AssetCallInfo(Action<T1, T2, T3> action) : base(action.Method, action.Target)
        {
        }
    }

    public class AssetCallInfo<T1, T2, T3, T4> : CallInfo
    {
        public AssetCallInfo(Action<T1, T2, T3, T4> action) : base(action.Method, action.Target)
        {
        }
    }

    public class AssetCallInfo<T1, T2, T3, T4, T5> : CallInfo
    {
        public AssetCallInfo(Action<T1, T2, T3, T4, T5> action) : base(action.Method, action.Target)
        {
        }
    }

    public class AssetCallInfo<T1, T2, T3, T4, T5, T6> : CallInfo
    {
        public AssetCallInfo(Action<T1, T2, T3, T4, T5, T6> action) : base(action.Method, action.Target)
        {
        }
    }

}