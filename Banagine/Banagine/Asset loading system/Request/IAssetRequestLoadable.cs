﻿namespace Banagine.Asset_loading_system.Request
{
    public interface IAssetRequestLoadable
    {
        AssetRequestLoader AssetRequestLoader { get; }
    }
}