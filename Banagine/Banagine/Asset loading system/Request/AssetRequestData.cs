﻿using System;

namespace Banagine.Asset_loading_system.Request
{
    public abstract class AssetRequestData
    {
        public string[] AssetNames;

        protected AssetRequestData(params string[] assetNames)
        {
            AssetNames = assetNames;
        }
    }

    public class AssetRequestData<TAsset> : AssetRequestData
    {
        public Type AssetType;

        public AssetRequestData(params string[] assetNames) : base(assetNames)
        {
            AssetType = typeof(TAsset);
        }
    }
}