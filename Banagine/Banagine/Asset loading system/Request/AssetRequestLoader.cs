﻿using System;
using System.Collections.Generic;
using System.Linq;
using Banagine.Asset_loading_system.Args;
using Banagine.EventSystem;

namespace Banagine.Asset_loading_system.Request
{
    public delegate void AssetRequestLoaded(AssetRequestResult assetRequestResult);

    public class AssetRequestLoader : IEventReceiver
    {
        private AssetRequestLoaded _onLoadedAllAssets;
        private CallInfo _callInfoLoadedAllAssets;
        private Dictionary<string, IAssetData> _loadedAssets;
        private int _assetCount;
        private AssetRequestData[] _assetRequestDatas;

        public IEnumerable<EventData> EventDatas => new []
        {
            new EventData<AssetLoadedEventArgs>(OnAssetLoaded, EngineEvent.AssetLoaded) 
        };

        public AssetRequestLoader(AssetRequestLoaded onLoadedAllAssets)
        {
            _onLoadedAllAssets = onLoadedAllAssets;
        }

        public AssetRequestLoader(CallInfo callInfoLoadedAllAssets)
        {
            _callInfoLoadedAllAssets = callInfoLoadedAllAssets;
        }

        private void Init(AssetRequestData[] assetRequestDatas)
        {
            _assetCount = 0;
            _loadedAssets = new Dictionary<string, IAssetData>();
            if (assetRequestDatas.Length > 0)
            {
                foreach (var assetRequestData in assetRequestDatas)
                {
                    foreach (var assetName in assetRequestData.AssetNames)
                    {
                        _loadedAssets.Add(assetName, null);
                        _assetCount++;
                    }
                }
                EventManager.AddEventReceiver(this);
                _assetRequestDatas = assetRequestDatas;
            }
            else
            {
                _assetRequestDatas = new AssetRequestData[0];
                Callback();
            }
        }

        public void Load(params AssetRequestData[] assetRequestDatas)
        {
            Init(assetRequestDatas);
            foreach (var assetRequestData in _assetRequestDatas)
            {
                foreach (var assetName in assetRequestData.AssetNames)
                {
                    Type assetType =
                        assetRequestData.GetType()
                        .GetField("AssetType")
                        .GetValue(assetRequestData) as Type;

                    EventManager.SendMessage(this, EngineEvent.LoadAsset, new AssetLoadingEventArgs(assetName, assetType, false));
                }
            }
        }

        private void OnAssetLoaded(object sender, AssetLoadedEventArgs e)
        {
            string assetName = e.AssetData.Name;
            if (_loadedAssets.ContainsKey(assetName))
            {
                _loadedAssets[assetName] = e.AssetData;
                if (!_loadedAssets.ContainsValue(null))
                {
                    Callback();
                    EventManager.RemoveEventReceiver(this);
                }
            }

        }

        private void Callback()
        {
            List<IAssetData> loadedAssets = _loadedAssets.Select(item => item.Value).ToList();
            _onLoadedAllAssets?.Invoke(new AssetRequestResult(loadedAssets));
            _callInfoLoadedAllAssets?.Call(loadedAssets.ToArray());
        }
    }
}