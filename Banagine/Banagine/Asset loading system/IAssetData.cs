﻿namespace Banagine.Asset_loading_system
{
    public interface IAssetData
    {
        string Name { get; }
    }
}