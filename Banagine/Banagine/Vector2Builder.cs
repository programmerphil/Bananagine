﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Single;

namespace Banagine
{
    public class Vector2Builder
    {
        public static Vector<float> Create(float x, float y)
        {
            return DenseVector.OfArray(new [] { x, y });
        }
    }
}