﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Banagine.Asset_loading_system;
using Banagine.Asset_loading_system.Loader;
using Banagine.Asset_loading_system.Loader.Shader;
using Banagine.Asset_loading_system.Loader.Sound;
using Banagine.Asset_loading_system.Loader.Texture;
using Banagine.CollisionSystem;
using Banagine.ConfigSystem;
using Banagine.EventSystem;
using Banagine.InputSystem;
using Banagine.Rendering_system;
using Banagine.Rendering_system.Assets;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.OpenTk;
using Banagine.Scene_system;
using Banagine.SoundSystem;
using MathNet.Numerics.LinearAlgebra;
using Pipeline;

namespace Banagine
{
    public class Engine
    {
        private const string ConfigFileName = "engine.configuration";

        private IWindow<WindowConfigData> _window;
        private ConfigConvert _configConvert;

        public Engine()
        {
            _configConvert = new ConfigConvert();
        }

        private void CreateDefaultConfigFile(string filePath)
        {
            EngineConfig engineConfig = new EngineConfig()
            {
                SoundAssetLoaderConfigData = new AssetLoaderConfigData<ISound>(FolderPaths.CreateSoundAssetFilePath(Environment.CurrentDirectory), AssetLoadType.PreLoaded),
                TextureAssetLoaderConfigData = new AssetLoaderConfigData<ITextureData>(FolderPaths.CreateTexureAssetFilePath(Environment.CurrentDirectory), AssetLoadType.PreLoaded),
                ShaderAssetLoaderConfigData = new AssetLoaderConfigData<IShaderData>(FolderPaths.CreateShaderAssetFilePath(Environment.CurrentDirectory), AssetLoadType.PreLoaded)
            };

            _configConvert.Serialize(engineConfig, filePath);
        }

        public void Init(IGame game, WindowConfigData config)
        {
            EngineConfig engineConfig =
                _configConvert.DeSerialize<EngineConfig>(Environment.CurrentDirectory + @"\" + ConfigFileName);
            EventManager.Init(true, new TestEventsFinder(), new AssetLoaderEventsFinder(
                engineConfig.SoundAssetLoaderConfigData.Load(Environment.CurrentDirectory, new SoundFactory()),
                engineConfig.TextureAssetLoaderConfigData.Load(Environment.CurrentDirectory, new TextureFactory()),
                engineConfig.ShaderAssetLoaderConfigData.Load(Environment.CurrentDirectory, new ShaderFactory())),
                new SceneManager(),
                new Time(),
                new SoundMessagingSystem(new IrrKlangSoundManager()),
                new GameEvents(game),
                new Input(
                    new List<IPullInputManager>() { new PcKeyboardPullInputManager(), new MousePositionInputManager() },
                    new List<IPushInputManager>(){ new MousePushInputManager() }));
            _window = new OpenTkWindow();
            _window.Init(config);
            EventManager.SendMessage(this, EngineEvent.Init, true);
            EventManager.SendMessage(this, EngineEvent.LoadAssets, true);
        }

        public void Run()
        {
            _window.Run(30);
        }
    }
}