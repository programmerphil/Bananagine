﻿using GlmNet;
using OpenTK;

namespace Banagine.Utillity
{
    public static class Matrix4Converter
    {
        public static Matrix4 ToMatrix4(mat4 from)
        {
            return new Matrix4(
                new Vector4(from[0].x, from[0].y, from[0].z, from[0].w),
                new Vector4(from[1].x, from[1].y, from[1].z, from[1].w),
                new Vector4(from[2].x, from[2].y, from[2].z, from[2].w),
                new Vector4(from[3].x, from[3].y, from[3].z, from[3].w));
        }
    }
}