namespace Banagine.Assets.Shaders
{
    public class Shaders
    {
        public const string DefaultFragmentShader = "DefaultFragmentShader";
        public const string DefaultTextureFragmentShader = "DefaultTextureFragmentShader";
        public const string DefaultVertexPolygonShader = "DefaultVertexPolygonShader";
        public const string DefaultVertexTextureShader = "DefaultVertexTextureShader";
    }
}