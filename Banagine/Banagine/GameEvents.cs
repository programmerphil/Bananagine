﻿using System;
using System.Collections.Generic;
using Banagine.EventSystem;

namespace Banagine
{
    public class GameEvents :  IEventReceiver, IEventsFinder
    {
        private IGame _game;

        public IEnumerable<EventData> EventDatas => new List<EventData>()
        {
            new EventData<EventArgs>(Init, EngineEvent.Init),
            new EventData<EventArgs>(LoadAssets, EngineEvent.LoadAssets),
            new EventData<EventArgs>(Update, EngineEvent.Update),
            new EventData<EventArgs>(Render, EngineEvent.Render),
        };

        public GameEvents(IGame game)
        {
            _game = game;
        }

        public void Init(object sender, EventArgs e)
        {
            _game.Init();
        }

        public void LoadAssets(object sender, EventArgs e)
        {
            _game.LoadAssets();
        }

        public void Update(object sender, EventArgs e)
        {
            _game.Update();
        }

        public void Render(object sender, EventArgs e)
        {
            _game.Render();
        }

        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            return new List<IEventReceiver>() { this };
        }
    }
}