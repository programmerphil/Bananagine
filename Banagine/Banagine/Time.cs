﻿using System;
using System.Collections.Generic;
using Banagine.EventSystem;

namespace Banagine
{
    public class Time : Updateable
    {
        public static float DeltaTime;
        public static float TimeSinceStart;

        private DateTime _lastTime;

        public Time()
        {
            _lastTime = DateTime.Now;
        }

        public override void Update(object sender, EventArgs e)
        {
            DeltaTime = (float)(DateTime.Now - _lastTime).Milliseconds / 1000;
            TimeSinceStart += DeltaTime;

            _lastTime = DateTime.Now;
        }
    }
}