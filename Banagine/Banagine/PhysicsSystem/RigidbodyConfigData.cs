using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.PhysicsSystem
{
	public class RigidbodyConfigData
	{
	    public Vector<float> Position { get; }
	    public float Rotation { get; }
	    public float Mass { get; }
	    public float Gravity { get; }
	    public float Drag { get; }
	    public float AngularDrag { get; }
        public bool UseGravity { get; }

	    public RigidbodyConfigData(Vector<float> position = null, float rotation = 0, float drag = 0, float angularDrag = 0, float mass = 1, float gravity = 9.82f, bool useGravity = true)
	    {
	        Drag = drag;
	        AngularDrag = angularDrag;
	        Position = position ?? Vector2Builder.Create(0, 0);
	        Rotation = rotation;
	        Mass = mass;
	        Gravity = gravity;
	        UseGravity = useGravity;
	    }
	}
}
