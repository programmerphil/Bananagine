using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.PhysicsSystem
{
	public interface IRigidbody
	{
		Vector<float> Velocity { get; }
        Vector<float> Position { get; }

        float AngularVelocity { get; }
        float Rotation { get; }
        float Mass { get; set; }
        float Gravity { get; set; }
        float Drag { get; set; }
        float AngularDrag { get; set; }

        bool UseGravity { get; set; }
        bool IsKinetic { get; set; }

        void AddForce(Vector<float> force);

		void AddRelativeForce(Vector<float> force);

		void AddTorque(float torque);
	}
}
