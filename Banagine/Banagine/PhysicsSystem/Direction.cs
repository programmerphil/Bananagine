﻿namespace Banagine.PhysicsSystem
{
    public enum Direction
    {
        Left,
        Right, 
        Up,
        Down
    }
}