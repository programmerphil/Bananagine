using System;
using System.Collections.Generic;
using System.Linq;
using Banagine.CollisionSystem;
using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.PhysicsSystem
{
	public class Rigidbody2D : IRigidbody
	{
	    private const float MinProcentSpeedAfterMaxFriction = 0.25f;
	    private const float Tolerance = 10;

	    private Collider _collider;
        private List<Direction> _inMoveableDirections;
        private float _currentFriction;
        private List<ICollider> _currentCollidersCollidingWith;
        private float _drag;

        public Vector<float> Velocity { get; private set; }
        public Vector<float> Position { get; private set; }

	    public Vector<float> DeltaVelocity { get; private set; }
        public float DeltaRotation { get; private set; }

        public float AngularVelocity { get; private set; }
        public float Rotation { get; private set; }
        public float Mass { get; set; }
        public float Gravity { get; set; }

        /// <summary>
        /// Clamped between 0 and 1
        /// </summary>
	    public float Drag
	    {
	        get { return _drag; }
	        set
	        {
                _drag = MathExtensions.Clamp(value, 0, 1);
	        }
	    }

	    public float AngularDrag { get; set; }

        public bool UseGravity { get; set; }
        public bool IsKinetic { get; set; }

	    public Rigidbody2D(Collider collider, RigidbodyConfigData configData)
        {
            _collider = collider;
            Position = configData.Position;
            Rotation = configData.Rotation;
            Velocity = Vector2Builder.Create(0, 0);
            DeltaVelocity = Vector2Builder.Create(0, 0);
            Mass = configData.Mass;
            Gravity = configData.Gravity;
            Drag = configData.Drag;
            AngularDrag = configData.AngularDrag;
            UseGravity = configData.UseGravity;
            collider.CollisionStatusChange += OnCollisionChange;
            _inMoveableDirections = new List<Direction>();
            _currentCollidersCollidingWith = new List<ICollider>();
        }

	    private void OnCollisionChange(object sender, CollisionEventArgs e)
	    {
            CollisionData collisionData = e.CollisionData;
	        List<Direction> collisionDirections = GetCollisionDirections(collisionData.CollisionPoint);
            float otherColliderFriction = collisionData.OtherCollider.Friction;
            if (collisionData.Overlaps)
            {
                _currentFriction = Math.Max(_currentFriction, otherColliderFriction);
                _inMoveableDirections.AddRange(collisionDirections);
                _currentCollidersCollidingWith.AddIfNotExist(collisionData.OtherCollider);
            }
	        else
            {
                _currentCollidersCollidingWith.Remove(collisionData.OtherCollider);
                _inMoveableDirections.RemoveRange(collisionDirections);
                SetFrictionOnColliderExit();
            }
	    }

	    private void SetFrictionOnColliderExit()
	    {
            if (_currentCollidersCollidingWith.Count <= 0)
            {
                _currentFriction = 0;
            }
            else
            {
                _currentFriction = _currentCollidersCollidingWith.Max(item => item.Friction);
            }
        }

	    private List<Direction> GetCollisionDirections(Vector<float> collisionPoint)
	    {
            List<Direction> collisionDirections = new List<Direction>();

            if (collisionPoint.GetX() >= _collider.Left || IsLessThanTolerance(collisionPoint.GetX(), _collider.Left, Tolerance))
            {
                collisionDirections.AddIfNotExist(Direction.Right);
            }
            if (collisionPoint.GetX() <= _collider.Right || IsLessThanTolerance(collisionPoint.GetX(), _collider.Right, Tolerance))
            {
                collisionDirections.AddIfNotExist(Direction.Left);
            }
            if (collisionPoint.GetY() <= _collider.Bottom || IsLessThanTolerance(collisionPoint.GetY(), _collider.Bottom, Tolerance))
            {
                collisionDirections.AddIfNotExist(Direction.Up);
            }
            if (collisionPoint.GetY() >= _collider.Top)
            {
                collisionDirections.AddIfNotExist(Direction.Down);
            }

	        return collisionDirections;
	    }

	    private bool IsLessThanTolerance(float value1, float value2, float tolerance)
	    {
	        return Math.Abs(value1 - value2) < tolerance;

	    }

        public  void AddForce(Vector<float> force)
        {
            Velocity += (force / Mass) * Time.DeltaTime;
        }

		public  void AddRelativeForce(Vector<float> force)
		{
		    Velocity += ((force * RotationMatrix.Create(Rotation)) / Mass) * Time.DeltaTime;
		}

		public  void AddTorque(float torque)
		{
            AngularVelocity += (torque / Mass) * Time.DeltaTime;
		}

	    public void Update()
	    {
            DeltaVelocity = GetMoveableVelocity(Velocity.Clone()) * Math.Max(1 + MinProcentSpeedAfterMaxFriction - _currentFriction, 0);
	        DeltaRotation = AngularVelocity;
	        Position += DeltaVelocity;
            Rotation += DeltaRotation;

            AngularVelocity -= AngularDrag * Time.DeltaTime;
	        Velocity.SetY(Velocity.GetY() + Gravity * Time.DeltaTime);
            Vector<float> dragVelocity = (Drag * -Velocity) * Time.DeltaTime;

	        Velocity += dragVelocity;
	    }

	    private Vector<float> GetMoveableVelocity(Vector<float> velocity)
	    {
	        if (_inMoveableDirections.Contains(Direction.Right) && velocity.GetX() > 0 ||
                _inMoveableDirections.Contains(Direction.Left) && velocity.GetX() < 0)
	        {
	            velocity.SetX(0);
	        }
            if (_inMoveableDirections.Contains(Direction.Up) && velocity.GetY() < 0 ||
               _inMoveableDirections.Contains(Direction.Down) && velocity.GetY() > 0)
            {
                velocity.SetY(0);
            }

            return velocity;
	    }
	}
}
