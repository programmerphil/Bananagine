﻿using System;
using System.Drawing;
using Banagine.CollisionSystem;
using Banagine.EventSystem;
using Banagine.Rendering_system;
using Banagine.Scene_system;

namespace Banagine
{
    internal class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Engine engine = new Engine();
            engine.Init(new Game(), new WindowConfigData(1280, 720, "Banagine", Vector2Builder.Create(200, 200)));
            engine.Run();
        }
    }

    public class Game : IGame
    {
        public void Init()
        {
            EventManager.SendMessage(this, EngineEvent.LoadScene, new SceneLoadingEventArgs(new TestScene("Test", new SpatialHashMap(1000, 1000, new Size(100, 100)))));
        }

        public void LoadAssets()
        {

        }

        public void Update()
        {

        }

        public void Render()
        {

        }
    }
}