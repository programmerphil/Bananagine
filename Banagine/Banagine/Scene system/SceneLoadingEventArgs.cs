using System;
using System.Collections.Generic;

namespace Banagine.Scene_system
{
	public class SceneLoadingEventArgs : EventArgs
	{
	    private readonly string _sceneName;
	    private readonly Type _sceneType;
	    private readonly Scene _scene;

	    public string SceneName
	    {
	        get { return _sceneName; }
	    }

	    public Type SceneType
	    {
	        get { return _sceneType; }
	    }

	    public Scene Scene
	    {
	        get { return _scene; }
	    }

        public SceneLoadingEventArgs(string sceneName)
        {
            _sceneName = sceneName;
        }

        public SceneLoadingEventArgs(Type sceneType)
        {
            _sceneType = sceneType;
        }

        public SceneLoadingEventArgs(Scene scene)
        {
            _scene = scene;
        }

        public Scene GetSceneToLoad(List<Scene> scenes)
        {
            Scene scene = _scene;

		    if (scene != null)
		    {
		        return scene;
		    }

            LoadViaName(scenes, out scene);

            if (scene != null)
            {
                return scene;
            }

            LoadViaType(scenes, out scene);

            return Scene;
        }

	    private void LoadViaName(List<Scene> scenes, out Scene scene)
	    {
	        scene = scenes.Find(item => item.Name == SceneName);
	    }

	    private void LoadViaType(List<Scene> scenes, out Scene scene)
	    {
	        scene = scenes.Find(item => item.GetType() == SceneType);
	    }
	}
}
