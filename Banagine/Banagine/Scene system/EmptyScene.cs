﻿using Banagine.Asset_loading_system;
using Banagine.Asset_loading_system.Request;
using Banagine.CollisionSystem;

namespace Banagine.Scene_system
{
    public class EmptyScene : Scene
    {
        public EmptyScene(string name, ICollisionMap collisionMap) : base(name, collisionMap)
        {
        }

        public override AssetRequestData[] AssetRequestDatas => new AssetRequestData[] {null};

        protected override void Load()
        {
            
        }

        protected override void Setup(AssetRequestResult assetRequestResult)
        {
            
        }
    }
}