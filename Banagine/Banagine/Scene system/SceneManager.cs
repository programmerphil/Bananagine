using Banagine.CollisionSystem;
using Banagine.EventSystem;
using System.Collections.Generic;
using System.Drawing;

namespace Banagine.Scene_system
{
    public class SceneManager : IEventReceiver, IEventsFinder
    {
        public Scene ActiveScene;

        private List<Scene> _scenes;

        public IEnumerable<IEventReceiver> GetEventReceivers()
        {
            return new[]
            {
                ActiveScene  ?? new EmptyScene("Empty", new SpatialHashMap(1000, 1000, new Size(100, 100))),
                this as IEventReceiver,
            };
        }

        public IEnumerable<EventData> EventDatas => new EventData[]
        {
            new EventData<SceneLoadingEventArgs>(LoadScene, EngineEvent.LoadScene)
        };

        public SceneManager()
        {
            _scenes = new List<Scene>();
        }

        public void LoadScene(object sender, SceneLoadingEventArgs e)
        {
            ActiveScene?.Unload();
            Scene sceneToLoad = e.GetSceneToLoad(_scenes);
            sceneToLoad.Setup();
            ActiveScene = sceneToLoad;
        }
    }
}