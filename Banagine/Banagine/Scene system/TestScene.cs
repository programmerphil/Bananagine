﻿using System.Drawing;
using Banagine.Animation_system;
using Banagine.Assets.Audio;
using Banagine.Assets.Textures;
using Banagine.Asset_loading_system;
using Banagine.Asset_loading_system.Request;
using Banagine.CollisionSystem;
using Banagine.EC;
using Banagine.EC.Components;
using Banagine.EC.Components.Physics;
using Banagine.EC.Components.Physics.Collision;
using Banagine.EC.Components.Rendering;
using Banagine.PhysicsSystem;
using Banagine.Rendering_system.Assets;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.OpenTk;

namespace Banagine.Scene_system
{
    internal class TestScene : Scene
    {
        private GameObject _testObject;
        private GameObject _testObject2;

        public TestScene(string name, ICollisionMap collisionMap) : base(name, collisionMap)
        {
           
        }

        protected override void Setup(AssetRequestResult assetRequestResult)
        {
            _testObject = new GameObject(new Transform(Vector2Builder.Create(0, 0)));
            AnimatorComponent animatorComponent = new AnimatorComponent(1,
                new AnimationData("GG", new SpriteSheetAnimation(1, 2, new Size(32, 64))));
            _testObject.AddComponent(new SpriteRenderer(Textures.Banana, 100, 100))
                ;
            _testObject.AddComponent(new ColliderComponent(new BoxCollider(0, 100, 100, 0)));
            _testObject.AddComponent(new Rigidbody2DComponent(new RigidbodyConfigData(drag:0f)));

            _testObject2 = new GameObject(new Transform(Vector2Builder.Create(0, 420)));
            _testObject2.AddComponent(new SpriteRenderer(Textures.Banana, 100, 100));
            _testObject2.AddComponent(new ColliderComponent(new BoxCollider(0, 100, 100, 0)));
        }

        protected override void Load()
        {
           Add(_testObject);
           Add(_testObject2);
        }        
    }
}