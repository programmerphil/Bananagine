using System;
using System.Collections.Generic;
using System.Linq;
using Banagine.Asset_loading_system;
using Banagine.Asset_loading_system.Request;
using Banagine.CollisionSystem;
using Banagine.EC;
using Banagine.EC.Components;
using Banagine.EC.Components.Physics.Collision;
using Banagine.EC.Components.Rendering;
using Banagine.EventSystem;
using Banagine.Rendering_system;
using Banagine.Rendering_system.OpenTk;

namespace Banagine.Scene_system
{
	public abstract class Scene : IEventReceiver, IAssetRequestLoadable
	{
	    private CollisionManager _collisionManager;
	    private List<ICollider> _colliders;

        protected IRenderer _renderer;
	    protected Camera2D _camera2D;
		protected DelayedList<GameObject> _gameObjects;

	    public string Name { get; }

        public IEnumerable<EventData> EventDatas => new EventData[]
       {
            new EventData<EventArgs>(Render, EngineEvent.Render),
            new EventData<EventArgs>(Update, EngineEvent.Update)
       };

        public AssetRequestLoader AssetRequestLoader => new AssetRequestLoader(OnLoadedAssets);

        public virtual AssetRequestData[] AssetRequestDatas => new AssetRequestData[0];

        protected Scene(string name, ICollisionMap collisionMap)
	    {
	        Name = name;
            _collisionManager = new CollisionManager(collisionMap);
            _gameObjects = new DelayedList<GameObject>();
	        _camera2D = CreateCamera2D();
            _renderer = new DefaultRenderer(_camera2D);
            _colliders = new List<ICollider>();
        }

	    private Camera2D CreateCamera2D()
	    {
	        GameObject cameraObject = new GameObject(Vector2Builder.Create(0, 0));
            Camera2D camera2D = new Camera2D();
	        cameraObject.AddComponent(camera2D);
            Add(cameraObject);

	        return camera2D;
	    }

	    private void OnLoadedAssets(AssetRequestResult assetRequestResult)
	    {
	        Setup(assetRequestResult);
            Load();
	    }

        public void Update(object sender, EventArgs e)
		{
            _gameObjects.UpdateList();

            
          
            foreach (var gameObject in _gameObjects)
		    {
		        gameObject.Update();
            }

            _collisionManager.Update(_gameObjects.Select(item => item.GetComponent<ColliderComponent>()).Where(item => item != null).Cast<ICollider>().ToList());
        }

		public void Render(object sender, EventArgs e)
		{
		    foreach (var gameObject in _gameObjects)
		    {
		        gameObject.Render(_renderer);
		    }
		}

		public virtual void Unload()
		{
		    foreach (var gameObject in _gameObjects)
		    {
		        gameObject.OnRemoved();
		    }
            _gameObjects.RemoveAll();
            _gameObjects.UpdateList();
            EventManager.RemoveEventReceiver(this);
		}

		public void Add(GameObject gameObject)
		{
			_gameObjects.Add(gameObject);
		}

		public void Remove(GameObject gameObject)
		{
			_gameObjects.Remove(gameObject);
		}

	    public virtual void Setup()
	    {
            AssetRequestLoader.Load(AssetRequestDatas);
	    }

        protected abstract void Load();
	    protected abstract void Setup(AssetRequestResult assetRequestResult);
	}
}
