using System;
using System.Collections.Generic;
using System.Text;

namespace Banagine.SoundSystem
{
	public interface ISoundManager
	{
		float Volume { get; set; }

		void Play(string filePath, bool loop, float volume);

		void Play(string filePath, bool loop);

		void Play(ISound sound, bool loop);

		void Play(ISound sound, bool loop, float volume);

		void Stop(string fileName);

		void StopAll();
	}
}
