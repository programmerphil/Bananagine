﻿using System;

namespace Banagine.SoundSystem
{
    public class SoundNameEventArgs : EventArgs
    {
        public string FileName;

        public SoundNameEventArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}