using System;
using System.Collections.Generic;
using System.Text;
using Banagine.ExtensionsMethods;
using IrrKlang;

namespace Banagine.SoundSystem
{
	public class IrrKlangSound : ISound
	{
        public string Name { get; }

        public string FilePath { get; }
	    private IrrKlang.ISound _sound;

		public ISoundEngine IrrKlangSoundEngine { get; set; }

	    public IrrKlangSound(string filePath)
	    {
	        FilePath = filePath;
	        Name = FilePath.GetFileName();
	    }

		public ISound Play(bool loop, float volume)
		{
		    IrrKlangSoundEngine.SoundVolume = volume;
            _sound = IrrKlangSoundEngine.Play2D(FilePath, loop);
		    return this;
		}

		public void Stop()
		{
            _sound.Stop();
        }
	}
}
