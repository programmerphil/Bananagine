using System;
using System.Collections.Generic;
using System.Text;
using Banagine.Asset_loading_system;

namespace Banagine.SoundSystem
{
	public interface ISound : IAssetData
	{
		ISound Play(bool loop, float volume);

		void Stop();
	}
}
