﻿namespace Banagine.SoundSystem
{
    public enum SoundEvent
    {
        PlaySound,
        StopSound,
        StopAllSounds,
        SetSoundsVolume
    }
}