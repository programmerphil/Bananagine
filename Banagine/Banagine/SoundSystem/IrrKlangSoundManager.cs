using System;
using System.Collections.Generic;
using System.Text;
using Banagine.ExtensionsMethods;
using IrrKlang;
using MoreLinq;

namespace Banagine.SoundSystem
{
	public class IrrKlangSoundManager : ISoundManager
	{
	    public float Volume
	    {
	        get { return _soundEngine.SoundVolume; }
	        set { _soundEngine.SoundVolume = value; }
	    }

	    private ISoundEngine _soundEngine;
	    private Dictionary<string, ISound> _sounds;

	    public IrrKlangSoundManager()
	    {
            _soundEngine = new ISoundEngine();
            _sounds = new Dictionary<string, ISound>();
        }

	    public void Play(string filePath, bool loop, float volume)
	    {
	        IrrKlangSound irrKlangSound = new IrrKlangSound(filePath);
            Play(irrKlangSound, loop, volume);
        }

		public void Play(string filePath, bool loop)
		{
		    Play(filePath, loop, Volume);
		}

		public void Play(ISound sound, bool loop)
		{
			Play(sound, loop, Volume);
		}

		public void Play(ISound sound, bool loop, float volume)
		{
            IrrKlangSound irrKlangSound = sound as IrrKlangSound;
            irrKlangSound.IrrKlangSoundEngine = _soundEngine;
            irrKlangSound.Play(loop, volume);
            _sounds.Add(irrKlangSound.Name, irrKlangSound);
        }

		public void Stop(string fileName)
		{
			_sounds[fileName].Stop();
		    _sounds.Remove(fileName);
		}

		public void StopAll()
		{
			_sounds.ForEach(item => item.Value.Stop());
		}
	}
}
