﻿using Banagine.Asset_loading_system.Request;
using Banagine.EventSystem;
using System;
using System.Collections.Generic;

namespace Banagine.SoundSystem
{
    public class SoundMessagingSystem : EventFinder, IEventReceiver
    {
        public IEnumerable<EventData> EventDatas => new List<EventData>()
        {
            new EventData<SoundEventArgs>(PlaySound, SoundEvent.PlaySound),
            new EventData<SoundNameEventArgs>(StopSound, SoundEvent.StopSound),
            new EventData<EventArgs>(StopAllSounds, SoundEvent.StopAllSounds),
            new EventData<SoundVolumeEventArgs>(SetVolume, SoundEvent.SetSoundsVolume),
        };

        private AssetRequestLoader _assetRequestLoader;
        private SoundEventArgs _soundEventArgs;
        private ISoundManager _soundManager;

        public SoundMessagingSystem(ISoundManager soundManager)
        {
            _soundManager = soundManager;
            _assetRequestLoader = new AssetRequestLoader(new AssetCallInfo<ISound>(OnLoadedSound));
        }

        public void PlaySound(object sender, SoundEventArgs e)
        {
            _soundEventArgs = e;
            if (e.Sound == null)
            {
                _assetRequestLoader.Load(new AssetRequestData<ISound>(e.FileName));
            }
            else
            {
                PlaySound(e.Sound);
            }
        }

        private void OnLoadedSound(ISound sound)
        {
            PlaySound(sound);
        }

        private void PlaySound(ISound sound)
        {
            if (_soundEventArgs.Volume == -1)
            {
                _soundManager.Play(sound, _soundEventArgs.Looped);
            }
            else
            {
                _soundManager.Play(sound, _soundEventArgs.Looped, _soundEventArgs.Volume);
            }
        }

        public void StopSound(object sender, SoundNameEventArgs e)
        {
            _soundManager.Stop(e.FileName);
        }

        public void StopAllSounds(object sender, EventArgs e)
        {
            _soundManager.StopAll();
        }

        public void SetVolume(object sender, SoundVolumeEventArgs e)
        {
            _soundManager.Volume = e.Volume;
        }
    }
}