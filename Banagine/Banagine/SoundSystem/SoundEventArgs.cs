﻿using System;

namespace Banagine.SoundSystem
{
    public class SoundEventArgs : EventArgs
    {
        public string FileName;
        public ISound Sound;
        public bool Looped;
        public float Volume;

        public SoundEventArgs(string fileName, bool looped = false, float volume = -1)
        {
            FileName = fileName;
            Looped = looped;
            Volume = volume;
        }

        public SoundEventArgs(ISound sound, bool looped = false, float volume = -1)
        {
            Sound = sound;
            Looped = looped;
            Volume = volume;
        }
    }
}