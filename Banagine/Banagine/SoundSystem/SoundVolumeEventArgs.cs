﻿using System;

namespace Banagine.SoundSystem
{
    public class SoundVolumeEventArgs : EventArgs
    {
        public float Volume;

        public SoundVolumeEventArgs(float volume)
        {
            Volume = volume;
        }
    }
}