﻿using Banagine.Rendering_system.Drawables;

namespace Banagine.Animation_system
{
    public interface IAnimationRunData
    {
        int RunCount { get; }
        string Name { get; }
        float LatestUpdateTime { get; }

        void Update(float frameRate, Texture currentTexture);
        void SwitchImage();
        bool IsDone();
    }
}