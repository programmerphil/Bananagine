using System.Drawing;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.Animation_system
{
	public class SpriteSheetAnimation : IAnimation
	{
		private Rectangle[] _sourceRectangles;

	    public int Length => _sourceRectangles.Length;

		public SpriteSheetAnimation(Vector<float> startPositon, Size spriteSize, int length)
		{
            _sourceRectangles = new Rectangle[length];
		    for (int i = 0; i < length; i++)
		    {
		        _sourceRectangles[i] = new Rectangle(new Point((int)startPositon.GetX() + i * spriteSize.Width, (int)startPositon.GetY() + i * spriteSize.Height), spriteSize);
		    }
        }

		public SpriteSheetAnimation(params Rectangle[] sourceRectangles)
		{
		    _sourceRectangles = sourceRectangles;
		}

		public SpriteSheetAnimation(int cols, int rows, Size spriteSize)
		{
            _sourceRectangles = new Rectangle[cols * rows];
		    for (int col = 0; col < cols; col++)
		    {
		        for (int row = 0; row < rows; row++)
		        {
                    _sourceRectangles[row + col] = new Rectangle(new Point(spriteSize.Width * row , spriteSize.Height * col), spriteSize);
                }
		    }
        }

        public void SetTexture(Texture currentTexture, int index)
        {
            currentTexture.SourceRectangle = _sourceRectangles[index];
        }
	}
}
