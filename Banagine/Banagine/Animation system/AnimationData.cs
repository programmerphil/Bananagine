namespace Banagine.Animation_system
{
	public class AnimationData
	{

	    public string Name { get; }

		public IAnimation Animation { get; }

        public AnimationData(string name, IAnimation animation)
        {
            Name = name;
            Animation = animation;
        }
    }
}
