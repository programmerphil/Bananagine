﻿using System;
using Banagine.Rendering_system.Drawables;

namespace Banagine.Animation_system
{
    public class AnimationRunData : IAnimationRunData
    {
        private int _index;

        private IAnimation _animation;
        private AnimationData _animationData;
        private float _timerToUpdate;

        public int RunCount { get; private set; }
        public AnimationRunType RunType { get; }
        public float LatestUpdateTime { get; private set; }

        public string Name => _animationData.Name;

        public AnimationRunData(AnimationData animationData, AnimationRunType runType)
        {
            _animation = animationData.Animation;
            RunType = runType;
        }

        public void SetImage(Texture currentTexture)
        {
            _animation.SetTexture(currentTexture, _index);
        }

        public void Update(float frameRate, Texture currentTexture)
        {
            _timerToUpdate -= Time.DeltaTime;

            if (_timerToUpdate <= 0)
            {
                LatestUpdateTime = Time.TimeSinceStart;
                _timerToUpdate =  1 / frameRate;
                SwitchImage();
                SetImage(currentTexture);
            }
        }

        public void SwitchImage()
        {
            _index++;
            if (_index >= _animation.Length)
            {
                _index = 0;
                RunCount++;
            }
        }

        public bool IsDone()
        {
            switch (RunType)
            {
                case AnimationRunType.Loop:
                    return false;
                case AnimationRunType.Once:
                    return RunCount > 0;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        
    }
}