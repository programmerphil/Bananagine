using System;
using Banagine.EC;
using Banagine.EC.Components;
using Banagine.EC.Components.Rendering;
using Banagine.Rendering_system;

namespace Banagine.Animation_system
{
	public class AnimatorComponent : Component, IUpdateable
	{
		private SpriteRenderer _spriteRenderer;
		private Animator _animator;

		public AnimatorComponent(int frameRate, params AnimationData[] animationDatas)
		{
			_animator = new Animator(frameRate, animationDatas);
		}

		public void AddAnimation(AnimationData animationData)
		{
			_animator.AddAnimation(animationData);
		}

		public void PlayAnimation(string name, AnimationRunType runType)
		{
		    _animator.PlayAnimation(name, runType);
		}

		public void StopAllAnimations()
		{
			_animator.StopAllAnimations();
		}

		public void StopAnimation(string name)
		{
			_animator.StopAnimation(name);
		}

	    public override void OnAddedToGameObject()
	    {
	        base.OnAddedToGameObject();
	        _spriteRenderer = GetComponent<SpriteRenderer>();
	    }

	    public void Update()
	    {
	        if (_spriteRenderer.Texture != null)
	        {
                _animator.Update(_spriteRenderer.Texture);
            }
	    }
	}
}
