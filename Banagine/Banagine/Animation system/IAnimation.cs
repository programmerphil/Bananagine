using Banagine.Rendering_system.Drawables;

namespace Banagine.Animation_system
{
	public interface IAnimation
	{
		int Length { get; }

		void SetTexture(Texture currentTexture, int index);
	}
}
