using System.Drawing;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.OpenTk;

namespace Banagine.Animation_system
{
	public class MultipleSpriteAnimation : IAnimation
    {
	    private Texture[] _textures;

	    public int Length => _textures.Length;

        private int _textureCount;

        public void SetTexture(Texture currentTexture, int index)
        {
            currentTexture = _textures[index];
        }

		public MultipleSpriteAnimation(Size textureSize, params string[] imageFileNames)
		{
            TextureLoader textureLoader = new TextureLoader();
		    foreach (var imageFileName in imageFileNames)
		    {
		        textureLoader.Load(imageFileName, textureSize, OnLoadedTexture);
		    }
		}

        private void OnLoadedTexture(OpenTkTexture loadedTexture)
        {
            _textures[_textureCount] = loadedTexture;
            _textureCount++;
        }

		public MultipleSpriteAnimation(params Texture[] textures)
		{
		    _textures = textures;
		}
    }
}
