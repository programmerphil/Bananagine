using System.Collections.Generic;
using System.Linq;
using Banagine.EC;
using Banagine.Rendering_system.Drawables;

namespace Banagine.Animation_system
{
	public class Animator
	{
		private int _frameRate;
		private List<AnimationData> _animations;
	    private DelayedList<IAnimationRunData> _currentAnimations;

		public Animator(int framerate, params AnimationData[] animationDatas)
		{
		    _frameRate = framerate;
            _animations = new List<AnimationData>(animationDatas);
            _currentAnimations = new DelayedList<IAnimationRunData>();
		}

		public void AddAnimation(AnimationData animationData)
		{
            _animations.Add(animationData);
        }

		public void PlayAnimation(string name, AnimationRunType runType)
		{
			_currentAnimations.Add(new AnimationRunData(_animations.Find(item => item.Name == name), runType));
		}

		public void StopAllAnimations()
		{
			_currentAnimations.RemoveAll();
		}

		public void StopAnimation(string name)
		{
		    _currentAnimations.Remove(_currentAnimations.Find(item => item.Name == name));
		}

	    public void Update(Texture texture)
	    {
            _currentAnimations.UpdateList();

            foreach (var currentAnimation in _currentAnimations)
            {
                currentAnimation.Update(_frameRate, texture);
                if (currentAnimation.IsDone())
                {
                    _currentAnimations.Remove(currentAnimation);
                }
            }
	    }
	}
}
