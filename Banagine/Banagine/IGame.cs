﻿namespace Banagine
{
    public interface IGame
    {
        void Init();
        void LoadAssets();
        void Update();
        void Render();
    }
}