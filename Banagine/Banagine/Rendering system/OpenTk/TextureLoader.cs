﻿using System.Drawing;
using Banagine.Assets.Shaders;
using Banagine.Asset_loading_system.Request;
using Banagine.Rendering_system.Shading;

namespace Banagine.Rendering_system.OpenTk
{
    public delegate void OnLoadedTexture(OpenTkTexture texture);

    public class TextureLoader
    {
        private const string DefaultVertexShaderName = Shaders.DefaultVertexTextureShader;
        private const string DefaulFragmentShaderName = Shaders.DefaultTextureFragmentShader;

        private AssetRequestLoader _assetRequestLoader;
        private Size _textureSize;
        private Rectangle? _sourceRectangle;
        private OnLoadedTexture _onLoadedTexture;

        public void Load(string textureName, Size textureSize, OnLoadedTexture onLoadedTexture, Rectangle? sourceRectangle = null,
            string vertexShaderName = DefaultVertexShaderName, string fragmentShaderName = DefaulFragmentShaderName)
        {
            _textureSize = textureSize;
           
            _sourceRectangle = sourceRectangle;
            _onLoadedTexture = onLoadedTexture;
            _assetRequestLoader = new AssetRequestLoader(new AssetCallInfo<OpenTkTextureData, ShaderData, ShaderData>(OnLoadedAssets));
            _assetRequestLoader.Load(new AssetRequestData<OpenTkTextureData>(textureName), new AssetRequestData<ShaderData>(vertexShaderName, fragmentShaderName));
        }

        private void OnLoadedAssets(OpenTkTextureData textureData, ShaderData vertexShader, ShaderData fragmentShader)
        {
            _onLoadedTexture.Invoke(new OpenTkTexture(textureData, _textureSize.Width, _textureSize.Height, _sourceRectangle, vertexShader, fragmentShader));
        }
    }
}