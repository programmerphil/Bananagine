using System;
using System.Drawing;
using Banagine.Rendering_system.Drawables;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
	public class OpenTkWindow : GameWindow, IWindow<WindowConfigData>
	{
	    private Color4 _backgroundColor;
	    private Window _window;

        public void Init(WindowConfigData config)
        {
            Title = config.Title;
            Width = config.Width;
            Height = config.Height;
            Location = new Point((int)config.StartPosition[0],(int)config.StartPosition[1]);
            _window = new Window();
            GL.ClearColor(config.BackgroundColor);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            RenderFrame += Render;
        }

        public void Render(object sender, FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Render();
            
            SwapBuffers();
        }

	    public void Render()
	    {
            _window.Render();
        }

        protected override void OnResize(EventArgs e)
	    {
	        base.OnResize(e);
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
        }

	    protected override void OnUpdateFrame(FrameEventArgs e)
	    {
	        base.OnUpdateFrame(e);
            Update();
	    }


	    public void Update()
		{
			_window.Update();
		}
    }
}
