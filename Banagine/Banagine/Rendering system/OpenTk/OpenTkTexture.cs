using System;
using System.Drawing;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Assets;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.Shading;
using MathNet.Numerics.LinearAlgebra;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
	public class OpenTkTexture : Texture, IOpenTkDrawable<TextureVertex>
	{
        public Color Color = Color.White;

        private OpenTkRenderData<TextureVertex> _openTkRenderData;
	    private OpenTkTextureData _openTkTextureData;
	    private OpenTkRenderFactory _openTkRenderFactory;

        public OpenTkTexture(OpenTkTextureData openTkTextureData, int width, int height, Rectangle? sourceRectangle = null, params ShaderData[] shaderDatas) : 
            base(width, height, openTkTextureData.TextureHeight, openTkTextureData.TextureWidth, sourceRectangle)
        {
            _openTkRenderFactory = new OpenTkRenderFactory();
            _openTkRenderData = _openTkRenderFactory.GetOpenTkRenderData<TextureVertex>(OpenTkRenderType.Texture, shaderDatas);
            _openTkTextureData = openTkTextureData;
        }
	    
	    public override void Render(Vector<float> position, Matrix4 viewMatrix)
	    {
            _openTkRenderData.SetIndices(CreateIndices());
            _openTkRenderData.SetVertices(CreateVertices());
            _openTkTextureData.Bind();
            _openTkRenderData.Render(position, viewMatrix);
	    }

	    public int[] CreateIndices()
	    {
	        return OpenTkShapeDrawable.CreateIndicesFromPoints(Quad.CreateQuadPoints(0, Width, 0, Height));
	    }

	    public TextureVertex[] CreateVertices()
	    {
            RectangleF texCordRectangle = SourceRectangle.Divide(new Size(TextureWidth, TextureHeight));
            return new[]
                     {
                       new TextureVertex(Color, new Vector2(texCordRectangle.X, 1 - texCordRectangle.Y), new Vector2(0, 0)), // Top left
                       new TextureVertex(Color, new Vector2(texCordRectangle.X + texCordRectangle.Width, 1 - texCordRectangle.Y), new Vector2(Width, 0)), // Bottom Right
                       new TextureVertex(Color, new Vector2(texCordRectangle.X + texCordRectangle.Width, 1 - (texCordRectangle.Y + texCordRectangle.Height)), new Vector2(Width, Height)), // Bottom left
                       new TextureVertex(Color, new Vector2(texCordRectangle.X, 1 - (texCordRectangle.Y + texCordRectangle.Height)), new Vector2(0, Height)), // Top right

                   };
        }
	}
}
