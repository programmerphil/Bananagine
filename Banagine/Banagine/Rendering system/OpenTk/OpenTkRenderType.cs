﻿namespace Banagine.Rendering_system.OpenTk
{
    public enum OpenTkRenderType
    {
        Polygon,
        Texture
    }
}