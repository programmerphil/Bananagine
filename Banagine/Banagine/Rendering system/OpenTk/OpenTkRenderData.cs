﻿using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.Shading;
using Banagine.Utillity;
using GlmNet;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
    public class OpenTkRenderData<TVertex> : IDrawable where TVertex : struct, IVertex
    {
        private ShaderProgram _shaderProgram;
        private Matrix4Uniform _viewMatrix;
        private Matrix4Uniform _projectionMatrix;
        private Matrix4Uniform _modelMatrix;

        private ElementBuffer _elementBuffer;

        private VertexArray<TVertex> _vertexArray;

        public VertexBuffer<TVertex> VertexBuffer { get; set; }

        public OpenTkRenderData(ShaderProgram shaderProgram, VertexArray<TVertex> vertexArray, ElementBuffer elementBuffer, VertexBuffer<TVertex> vertexBuffer)
        {
            _shaderProgram = shaderProgram;
            _vertexArray = vertexArray;
            _elementBuffer = elementBuffer;
            _projectionMatrix = new Matrix4Uniform("projection", Matrix4Converter.ToMatrix4(glm.ortho(0, 1280, 720, 0)));
            _viewMatrix = new Matrix4Uniform("view", Matrix4.Zero); 
            _modelMatrix = new Matrix4Uniform("model", Matrix4.Identity);
            VertexBuffer = vertexBuffer;
        }

        public void SetIndices(int[] indices)
        {
            _elementBuffer.Indices = indices;
        }

        public void SetVertices(TVertex[] vertices)
        {
            VertexBuffer.ResetVertices();
            foreach (var vertex in vertices)
            {
                VertexBuffer.AddVertex(vertex);
            }
        }

        public void Render(Vector<float> position, Matrix4 viewMatrix)
        {
            _modelMatrix.Matrix = Matrix4.CreateTranslation(position.GetX(), position.GetY(), 0);
            _viewMatrix.Matrix = viewMatrix;
            _shaderProgram.Use();
            _modelMatrix.Set(_shaderProgram);
            _viewMatrix.Set(_shaderProgram);
            _projectionMatrix.Set(_shaderProgram);
            // bind vertex buffer and array objects
            VertexBuffer.Bind();
            _elementBuffer.Bind();
            
            _vertexArray.Bind();
            
            // upload vertices to GPU and draw them
            VertexBuffer.BufferData();
            _elementBuffer.BufferData();

            _elementBuffer.Draw();
        }
    }
}