﻿using System.Drawing;
using System.Drawing.Imaging;
using Banagine.Rendering_system.Assets;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
    public class OpenTkTextureData : ITextureData
    {
        public string Name { get; }
        public int TextureWidth => _image.Width;
        public int TextureHeight => _image.Height;

        public int TextureId { get; private set; }

        private Bitmap _image;

        public OpenTkTextureData(string name, Bitmap image)
        {
            Name = name;
            _image = image;
            LoadTexture();
        }

        public void LoadTexture()
        {
            GL.Enable(EnableCap.Texture2D);
            TextureId = GL.GenTexture();

            GL.BindTexture(TextureTarget.Texture2D, TextureId);
            BitmapData data = _image.LockBits(new Rectangle(0, 0, _image.Width, _image.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

            _image.UnlockBits(data);

            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public void Bind()
        {
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, TextureId);
        }

        public void UnBind()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }
    }
}