﻿using System;
using System.Diagnostics;
using Banagine.Rendering_system.Shading;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
    public class OpenTkRenderFactory
    {
        public OpenTkRenderData<TVertex> GetOpenTkRenderData<TVertex>(OpenTkRenderType renderType, params ShaderData[] shaderDatas) where TVertex : struct, IVertex
        {
            switch (renderType)
            {
                case OpenTkRenderType.Polygon:
                    Debug.Assert(!(typeof(TVertex) is ColouredVertex), "Wrong vertex");
                    return CreatePolygonOpenTkRenderData(shaderDatas) as OpenTkRenderData<TVertex>;
                case OpenTkRenderType.Texture:
                    Debug.Assert(!(typeof(TVertex) is TextureVertex), "Wrong vertex");
                    return CreateTextureOpenTkRenderData(shaderDatas) as OpenTkRenderData<TVertex>;
                default:
                    throw new ArgumentOutOfRangeException(nameof(renderType), renderType, null);
            }
        }

        private OpenTkRenderData<ColouredVertex> CreatePolygonOpenTkRenderData(params ShaderData[] shadersData)
        {
            OpenTkRenderDataBuilder<ColouredVertex> openTkRenderDataBuilder = new OpenTkRenderDataBuilder
                    <ColouredVertex>()
                .WithShaders(shadersData)
                .WithVertexAttribute(new VertexAttribute("vPosition", 2, VertexAttribPointerType.Float,
                    ColouredVertex.Size, 0))
                .WithVertexAttribute(new VertexAttribute("vColor", 4, VertexAttribPointerType.Float, ColouredVertex.Size,
                    12));

            return openTkRenderDataBuilder.Build();
        }

        private OpenTkRenderData<TextureVertex> CreateTextureOpenTkRenderData(params ShaderData[] shadersData)
        {
            OpenTkRenderDataBuilder<TextureVertex> openTkRenderDataBuilder = new OpenTkRenderDataBuilder
                    <TextureVertex>()
                .WithShaders(shadersData)
                .WithVertexAttribute(new VertexAttribute("position", 2, VertexAttribPointerType.Float,TextureVertex.Size, 0))
                .WithVertexAttribute(new VertexAttribute("color", 4, VertexAttribPointerType.Float, TextureVertex.Size, 8))
                .WithVertexAttribute(new VertexAttribute("texCoord", 2, VertexAttribPointerType.Float, TextureVertex.Size, 24));

            return openTkRenderDataBuilder.Build();
        }
    }
}