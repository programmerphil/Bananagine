﻿using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.Shading;
using MathNet.Numerics.LinearAlgebra;
using OpenTK;

namespace Banagine.Rendering_system.OpenTk
{
    public interface IOpenTkDrawable<TVertex> where TVertex : struct, IVertex
    {
        int[] CreateIndices();
        TVertex[] CreateVertices();
    }
}