﻿using System.Collections.Generic;
using System.Drawing;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.Shading;
using MathNet.Numerics.LinearAlgebra;
using OpenTK;
using OpenTK.Graphics;

namespace Banagine.Rendering_system.OpenTk
{
    public class OpenTkShapeDrawable : IDrawable
    {
        public List<Vector<float>> Points => _shape.Corners;

        private OpenTkRenderData<ColouredVertex> _openTkRenderData;
        private OpenTkRenderFactory _openTkRenderFactory;
        private IShape _shape;
        private Color _color;        

        public OpenTkShapeDrawable(Color color, ShaderData[] shaderDatas, IShape shape)
        {
            _color = color;
            _shape = shape;
            _openTkRenderFactory = new OpenTkRenderFactory();
            _openTkRenderData = _openTkRenderFactory.GetOpenTkRenderData<ColouredVertex>(OpenTkRenderType.Polygon, shaderDatas);
        }

        public ColouredVertex[] CreateVertices()
        {
            ColouredVertex[] colouredVertices = new ColouredVertex[Points.Count];

            for (int i = 0; i < Points.Count; i++)
            {
                Vector<float> normalPosition = Points[i];
                colouredVertices[i] = new ColouredVertex(new Vector2(normalPosition[0], normalPosition[1]), new Color4(_color.R, _color.G, _color.B, _color.A));
            }

            return colouredVertices;
        }

        public int[] CreateIndices()
        {
            return CreateIndicesFromPoints(Points.ToArray());
        }

        public void Render(Vector<float> position, Matrix4 viewMatrix)
        {
            _openTkRenderData.SetIndices(CreateIndices());
            _openTkRenderData.SetVertices(CreateVertices());
            _openTkRenderData.Render(position, viewMatrix);
        }

        public static int[] CreateIndicesFromPoints(Vector<float>[] points)
        {
            int indicesCount = 0;
            int[] indices = new int[(points.Length - 1) * 3];
            for (int i = 1; i < points.Length; i += 1)
            {
                if (i + 1 < points.Length)
                {
                    indices[indicesCount] = 0;
                    indices[indicesCount + 1] = i;
                    indices[indicesCount + 2] = i + 1;
                    indicesCount += 3;
                }
            }

            return indices;
        }
    }
}
