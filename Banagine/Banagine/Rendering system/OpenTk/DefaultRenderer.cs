using System;
using Banagine.EC;
using Banagine.EC.Components;
using Banagine.EC.Components.Rendering;
using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
	public class DefaultRenderer : IRenderer
	{
	    private Camera2D _camera2D;

	    public DefaultRenderer(Camera2D camera2D)
	    {
	        _camera2D = camera2D;
	    }

		public void Render(Vector<float> position, IDrawable drawable)
		{
            drawable?.Render(position, _camera2D.GetViewMatrix());
		}
	}
}
