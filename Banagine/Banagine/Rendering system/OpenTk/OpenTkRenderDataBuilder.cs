﻿using System.Collections.Generic;
using Banagine.Rendering_system.Shading;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.OpenTk
{
    public class OpenTkRenderDataBuilder<TVertex> where TVertex : struct, IVertex
    {
        private List<ShaderData> _shaders;
        private VertexBuffer<TVertex> _vertexBuffer;
        private List<VertexAttribute> _vertexAttributes;
        private Matrix4Uniform _projectionMatrix;
        private ElementBuffer _elementBuffer;

        public OpenTkRenderDataBuilder()
        {
            _elementBuffer = new ElementBuffer();
            _vertexBuffer = new VertexBuffer<TVertex>();
            _shaders = new List<ShaderData>();
            _vertexAttributes = new List<VertexAttribute>();
        }

        public OpenTkRenderDataBuilder<TVertex> WithShader(ShaderData shaderData)
        {
            _shaders.Add(shaderData);
            return this;
        }

        public OpenTkRenderDataBuilder<TVertex> WithShaders(IEnumerable<ShaderData> shaders)
        {
            _shaders.AddRange(shaders);
            return this;
        }

        public OpenTkRenderDataBuilder<TVertex> WithVertexAttribute(VertexAttribute vertexAttribute)
        {
            _vertexAttributes.Add(vertexAttribute);
            return this;
        }

        public OpenTkRenderData<TVertex> Build()
        {
            ShaderProgram shaderProgram = new ShaderProgram(_shaders.ToArray());
            
            VertexArray<TVertex> vertexArray = new VertexArray<TVertex>(_vertexBuffer, _elementBuffer, shaderProgram, _vertexAttributes.ToArray());
            
            return new OpenTkRenderData<TVertex>(shaderProgram, vertexArray, _elementBuffer, _vertexBuffer);
        }
    }
}