﻿using System;

namespace Banagine.Rendering_system
{
    public class RenderEventArgs : EventArgs
    {
        public IRenderer Renderer;

        public RenderEventArgs(IRenderer renderer)
        {
            Renderer = renderer;
        }
    }
}