﻿using Banagine.Asset_loading_system;

namespace Banagine.Rendering_system.Assets
{
    public interface ITextureData : IAssetData
    {
        void LoadTexture();
    }
}