﻿using Banagine.Asset_loading_system;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Assets
{
    public interface IShaderData : IAssetData
    {
        void LoadShader(string code, ShaderType shaderType, out int address);
    }
}