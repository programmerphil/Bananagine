using Banagine.Rendering_system.Drawables;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Single;
using OpenTK;

namespace Banagine.Rendering_system
{
	public interface IRenderer
	{
		void Render(Vector<float> position, IDrawable drawable);
	}
}
