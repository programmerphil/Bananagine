namespace Banagine.Rendering_system
{
	public interface IWindow<TWindowConfigData>
	{
	    void Init(TWindowConfigData config);

		void Render();

		void Update();

		void Run(double updatesPerSecound, double framesPerSecound);

        void Run(double updatesPerSecound);
    }
}
