using System;
using System.Drawing;
using MathNet.Numerics.LinearAlgebra;
using OpenTK.Graphics;

namespace Banagine.Rendering_system
{
	public class WindowConfigData
	{
	    public int Width { get; }
        public int Height { get; }
        public string Title { get; }
        public Vector<float> StartPosition { get; }
        public Color BackgroundColor { get; }

        public WindowConfigData(int width = 0, int height = 0, string title = "", Vector<float> startPosition = null, Color? color = null)
        {
            Width = width;
            Height = height;
            Title = title;
            StartPosition = startPosition;
            BackgroundColor = color ?? Color.CornflowerBlue;
        }
    }
}
