using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Banagine.CollisionSystem;
using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Drawables
{
	public class Polygon : Shape, IShape<Polygon, Quad, Circle>
	{
	    private List<Vector<float>> _corners;
	    private PolygonCollision _polygonCollision;

	    public override List<Vector<float>> Corners
	    {
	        get { return _corners; }
	        protected set
	        {
	            _corners = value;
                BuildEdges();
            }
	    }

        // Taken from https://www.codeproject.com/Articles/15573/D-Polygon-Collision-Detection.
        public Vector<float> Center
        {
            get
            {
                float totalX = 0;
                float totalY = 0;
                for (int i = 0; i < Corners.Count; i++)
                {
                    totalX += Corners[i].GetX();
                    totalY += Corners[i].GetY();
                }

                return Vector2Builder.Create(totalX / (float)Corners.Count, totalY / (float)Corners.Count);
            }
        }

        public List<Vector<float>> Edges { get; private set; }

	    public Polygon(Vector<float>[] corners)
	    {
            _polygonCollision = new PolygonCollision();
            Init(corners);
        }

	    public bool OverlapsWith(Polygon otherPolygon)
	    {
            return _polygonCollision.CheckCollision(this, otherPolygon, null).Intersect;
	    }

        public bool OverlapsWith(Quad quad)
        {
            return quad.OverlapsWith(this);
        }

        public bool OverlapsWith(Circle circle)
        {
            return circle.OverlapsWith(this);
        }

        // Taken from https://www.codeproject.com/Articles/15573/D-Polygon-Collision-Detection.
        public override string ToString()
        {
            string result = "";

            for (int i = 0; i < Corners.Count; i++)
            {
                if (result != "") result += " ";
                result += "{" + Corners[i].ToString() + "}";
            }

            return result;
        }

        // Taken from https://www.codeproject.com/Articles/15573/D-Polygon-Collision-Detection.
        private void BuildEdges()
        {
            Edges.Clear();
            for (int i = 0; i < Corners.Count; i++)
            {
                var fromCorner = Corners[i];
                Vector<float> toCorner;
                if (i + 1 >= Corners.Count)
                {
                    toCorner = Corners[0];
                }
                else
                {
                    toCorner = Corners[i + 1];
                }
                Edges.Add(toCorner - fromCorner);
            }
        }
	}
}
