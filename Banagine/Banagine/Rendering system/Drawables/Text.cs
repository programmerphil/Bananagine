using MathNet.Numerics.LinearAlgebra;
using OpenTK;

namespace Banagine.Rendering_system.Drawables
{
	public abstract class Text : IDrawable
	{
	    public string DisplayText { get; set; }

	    public Alignment Alignment { get; set; }

        public int FontSize { get; set; }

	    public abstract int Width { get; }

	    public abstract int Height { get; }

        protected Text(string displayText, Alignment alignment = Alignment.Center, int fontSize = 12)
        {
            DisplayText = displayText;
            Alignment = alignment;
            FontSize = fontSize;
        }

	    public abstract void Render(Vector<float> position, Matrix4 viewMatrix);
	}
}
