using System.Drawing;
using Banagine.Asset_loading_system;
using Banagine.ExtensionsMethods;
using Banagine.Rendering_system.OpenTk;
using MathNet.Numerics.LinearAlgebra;
using OpenTK;

namespace Banagine.Rendering_system.Drawables
{
	public abstract class Texture : IDrawable
	{
	    public int TextureHeight { get; protected set; }
        public int TextureWidth { get; protected set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public Rectangle SourceRectangle { get; set; }

        protected Texture(int width, int height, int textureHeight, int textureWidth, Rectangle? sourceRectangle = null)
	    {
            Width = width;
            Height = height;
	        TextureHeight = textureHeight;
	        TextureWidth = textureWidth;
	        SourceRectangle = sourceRectangle ?? new Rectangle(0, 0, TextureWidth, TextureHeight);
	    }

        public abstract void Render(Vector<float> position, Matrix4 viewMatrix);
	}
}
