﻿using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.Rendering_system.Drawables
{
    public class Shape : IShape
    {
        private Vector<float> _position;

        public virtual List<Vector<float>> Corners { get; protected set; }

        public Vector<float> Position
        {
            get { return _position; }
            set
            {
                OnPositionChange(value);
                for (int i = 0; i < Corners.Count; i++)
                {
                    Corners[i] = _startCorners[i] + value;
                }
                _position = value;
            }
        }

        protected List<Vector<float>> _startCorners;

        protected void Init(Vector<float>[] corners)
        {
            _startCorners = new List<Vector<float>>();
            foreach (var corner in corners)
            {
                _startCorners.Add(corner.Clone());
            }
            Corners = corners.ToList();
        }

        protected virtual void OnPositionChange(Vector<float> newPosition)
        {
            
        }
    }
}