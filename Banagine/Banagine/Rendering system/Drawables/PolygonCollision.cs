﻿using System;
using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;

namespace Banagine.Rendering_system.Drawables
{
    // Taken from https://www.codeproject.com/Articles/15573/D-Polygon-Collision-Detection with small modifications.
    public class PolygonCollision
    {
        public PolygonCollisionResult CheckCollision(Polygon polygonA,
                              Polygon polygonB, Vector<float> velocity)
        {
            PolygonCollisionResult result = new PolygonCollisionResult();
            result.Intersect = true;
            result.WillIntersect = true;

            int edgeCountA = polygonA.Corners.Count;
            int edgeCountB = polygonB.Corners.Count;
            float minIntervalDistance = float.PositiveInfinity;
            Vector<float> translationAxis = Vector2Builder.Create(0, 0);
            Vector<float> edge;

            // Loop through all the edges of both polygons
            for (int edgeIndex = 0; edgeIndex < edgeCountA + edgeCountB; edgeIndex++)
            {
                if (edgeIndex < edgeCountA)
                {
                    edge = polygonA.Corners[edgeIndex];
                }
                else
                {
                    edge = polygonB.Corners[edgeIndex - edgeCountA];
                }

                // ===== 1. Find if the polygons are currently intersecting =====

                // Find the axis perpendicular to the current edge
                Vector<float> axis = Vector2Builder.Create(-edge.GetY(), edge.GetX());
                axis.Normalize(1);

                // Find the projection of the polygon on the current axis
                float minA = 0; float minB = 0; float maxA = 0; float maxB = 0;
                ProjectPolygon(axis, polygonA, ref minA, ref maxA);
                ProjectPolygon(axis, polygonB, ref minB, ref maxB);

                // Check if the polygon projections are currentlty intersecting
                if (IntervalDistance(minA, maxA, minB, maxB) > 0)
                {
                    result.Intersect = false;
                }

                // ===== 2. Now find if the polygons *will* intersect =====
                if (velocity != null)
                {
                    ProjectVelocity(velocity, axis, ref minA, ref maxA);

                    // Do the same test as above for the new projection
                    float intervalDistance = IntervalDistance(minA, maxA, minB, maxB);
                    if (intervalDistance > 0) result.WillIntersect = false;

                    // If the polygons are not intersecting and won't intersect, exit the loop
                    if (!result.Intersect && !result.WillIntersect) break;

                    // Check if the current interval distance is the minimum one. If so store
                    // the interval distance and the current distance.
                    // This will be used to calculate the minimum translation vector
                    intervalDistance = Math.Abs(intervalDistance);
                    if (intervalDistance < minIntervalDistance)
                    {
                        minIntervalDistance = intervalDistance;
                        translationAxis = axis;

                        Vector<float> d = polygonA.Center - polygonB.Center;
                        if (d.DotProduct(translationAxis) < 0)
                            translationAxis = -translationAxis;
                    }

                    // The minimum translation vector
                    // can be used to push the polygons appart.
                    if (result.WillIntersect)
                        result.MinimumTranslationVector =
                               translationAxis * minIntervalDistance;
                }

                
            }
            return result;
        }

        private  void ProjectVelocity(Vector<float> velocity, Vector<float> axis, ref float minA, ref float maxA)
        {
        // Project the velocity on the current axis
            float velocityProjection = axis.DotProduct(velocity);

            // Get the projection of polygon A during the movement
            if (velocityProjection < 0)
            {
                minA += velocityProjection;
            }
            else
            {
                maxA += velocityProjection;
            }
        }

        public void ProjectPolygon(Vector<float> axis, Polygon polygon,
                           ref float min, ref float max)
        {
            // To project a point on an axis use the dot product
            float dotProduct = axis.DotProduct(polygon.Corners[0]);
            min = dotProduct;
            max = dotProduct;
            for (int i = 0; i < polygon.Corners.Count; i++)
            {
                dotProduct = polygon.Corners[i].DotProduct(axis);
                if (dotProduct < min)
                {
                    min = dotProduct;
                }
                else
                {
                    if (dotProduct > max)
                    {
                        max = dotProduct;
                    }
                }
            }
        }

        public float IntervalDistance(float minA, float maxA, float minB, float maxB)
        {
            if (minA < minB)
            {
                return minB - maxA;
            }
            else
            {
                return minA - maxB;
            }
        }
    }
}