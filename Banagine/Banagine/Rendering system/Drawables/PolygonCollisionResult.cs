﻿using MathNet.Numerics.LinearAlgebra;

namespace Banagine.Rendering_system.Drawables
{
    // Taken from https://www.codeproject.com/Articles/15573/D-Polygon-Collision-Detection.
    public struct PolygonCollisionResult
    {
        // Are the polygons going to intersect forward in time?
        public bool WillIntersect;
        // Are the polygons currently intersecting?
        public bool Intersect;
        // The translation to apply to the first polygon to push the polygons apart.
        public Vector<float> MinimumTranslationVector;
    }
}