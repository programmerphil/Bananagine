using System;
using System.Collections.Generic;
using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;

namespace Banagine.Rendering_system.Drawables
{
	public class Circle : Shape, IShape<Quad, Polygon, Circle>
	{
	    private const int CornerCount = 15;
	    private const int DegreesInCircle = 360;

        public float Radius { get; set; }

	    public Vector<float> Center { get; set; }

	    public Circle(float radius, Vector<float> center)
	    {
	        Radius = radius;
	        Center = center;
            Corners = new List<Vector<float>>();
            MakeCorners();
            Init(Corners.ToArray());
	    }

	    public void MakeCorners()
	    {
	        int angleIncrease = DegreesInCircle / CornerCount;
	        for (int i = 0; i < CornerCount; i++)
	        {
                Corners.Add(Vector2Builder.Create((float)(Center.GetX() + (Radius * Math.Cos(angleIncrease * i))), (float)(Center.GetY() + (Radius * Math.Sin(angleIncrease * i)))));
            }
        }

        public bool OverlapsWith(Quad quad)
        {
            Vector<float> circleDistance = Vector2Builder.Create(0, 0);

            circleDistance.SetX(Math.Abs(Center.GetX() - quad.Right / 2));
            circleDistance.SetY(Math.Abs(Center.GetY() - quad.Top / 2));

            if (circleDistance.GetX() > (quad.Width / 2 + Radius))
            {
                return false;
            }
            if (circleDistance.GetY() > (quad.Height / 2 + Radius))
            {
                return false;
            }

            if (circleDistance.GetX() <= (quad.Width / 2))
            {
                return true;
            }
            if (circleDistance.GetY() <= (quad.Height / 2))
            {
                return true;
            }

            float cornerDistanceSquared = (float)(Math.Pow(circleDistance.GetX() - quad.Width / 2, 2) +
                                 Math.Pow(circleDistance.GetY() - quad.Height / 2, 2));

            return (cornerDistanceSquared <= (Math.Pow(Radius, 2)));
        }

	    public bool OverlapsWith(Polygon polygon)
	    {
	        return polygon.OverlapsWith(new Polygon(Corners.ToArray()));
	    }

        // http://stackoverflow.com/questions/8524722/circle-collision-formula
        public bool OverlapsWith(Circle circle)
	    {
            var radius = Radius + circle.Radius;
            var deltaX = Center.GetX() - circle.Center.GetX();
            var deltaY = Center.GetY() - circle.Center.GetY();
            return deltaX * deltaX + deltaY * deltaY <= radius * radius;
        }
	}
}
