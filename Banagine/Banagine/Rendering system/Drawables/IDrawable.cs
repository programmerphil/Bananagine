using MathNet.Numerics.LinearAlgebra;
using OpenTK;

namespace Banagine.Rendering_system.Drawables
{
	public interface IDrawable
	{
		void Render(Vector<float> position, Matrix4 viewMatrix);
	}
}
