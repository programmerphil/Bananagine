using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Banagine.CollisionSystem;
using Banagine.ExtensionsMethods;
using MathNet.Numerics.LinearAlgebra;
using OpenTK;

namespace Banagine.Rendering_system.Drawables
{
	public class Quad : Shape, IShape<Quad, Polygon, Circle>
	{
	    private RectangleF _rectangle;

	    public RectangleF Rectangle
	    {
	        get { return _rectangle; }
	        private set { _rectangle = value; }
	    }

	    public float Top => Rectangle.Top;
        public float Bottom => Rectangle.Bottom;
        public float Left => Rectangle.Left;
        public float Right => Rectangle.Right;
	    public float Width => Rectangle.Width;
	    public float Height => Rectangle.Height;

        public Quad(float left, float right, float top, float bottom)
	    {
	        Rectangle = new RectangleF(left, top, right - left, bottom - top);
            Init(CreateQuadPoints(left, right, top, bottom));
	    }      

        public static Vector<float>[] CreateQuadPoints(float left, float right, float top, float bottom)
	    {
	        return new []
	        {
                Vector2Builder.Create(left, top),
                Vector2Builder.Create(left, bottom),
                Vector2Builder.Create(right, bottom),
                Vector2Builder.Create(right, top)
            };
	    }

	    public bool OverlapsWith(Quad quad)
	    {
	        return Rectangle.IntersectsWith(quad.Rectangle);
	    }

	    public bool OverlapsWith(Polygon polygon)
	    {
	        return new Polygon(Corners.ToArray()).OverlapsWith(polygon);
	    }

	    public bool OverlapsWith(Circle circle)
	    {
	        return circle.OverlapsWith(this);
	    }

	    protected override void OnPositionChange(Vector<float> newPosition)
	    {
            _rectangle.Location = new PointF(newPosition.GetX(), newPosition.GetY());
	    }
	}
}
