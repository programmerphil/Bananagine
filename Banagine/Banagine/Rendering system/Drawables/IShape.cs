﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banagine.Rendering_system.Drawables
{
    public interface IShape
    {
        List<Vector<float>> Corners { get; }
        Vector<float> Position { get; set; }
    }

    public interface IShape<TOverlapShape1> : IShape
    {
        bool OverlapsWith(TOverlapShape1 overlapShape1);
    }

    public interface IShape<TOverlapShape1, TOverlapShape2> : IShape
    {
        bool OverlapsWith(TOverlapShape1 overlapShape1);
        bool OverlapsWith(TOverlapShape2 polygon);
    }

    public interface IShape<TOverlapShape1, TOverlapShape2, TOverlapShape3> : IShape
    {
        bool OverlapsWith(TOverlapShape1 overlapShape1);
        bool OverlapsWith(TOverlapShape2 overlapShape2);
        bool OverlapsWith(TOverlapShape3 overlapShape3);
    }

    public interface IShape<TOverlapShape1, TOverlapShape2, TOverlapShape3, TOverlapShape4> : IShape
    {
        bool OverlapsWith(TOverlapShape1 overlapShape1);
        bool OverlapsWith(TOverlapShape2 overlapShape2);
        bool OverlapsWith(TOverlapShape3 overlapShape3);
        bool OverlapsWith(TOverlapShape4 overlapShape4);
    }
}
