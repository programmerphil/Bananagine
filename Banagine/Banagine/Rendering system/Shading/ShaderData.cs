using Banagine.Rendering_system.Assets;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
	public class ShaderData : IShaderData
    {
        public string Name { get; private set; }

        private int _shaderAddress;

	    public int Address => _shaderAddress;

        public ShaderData(string name, string shader, ShaderType shaderType)
        {
            Name = name;
            LoadShader(shader, shaderType, out _shaderAddress);
        }

	    public void LoadShader(string code, ShaderType shaderType, out int address)
	    {
	        address = GL.CreateShader(shaderType);
            GL.ShaderSource(address, code);
            GL.CompileShader(address);
            System.Console.WriteLine("Shader log info" + GL.GetShaderInfoLog(address));
	    }
	}
}
