using System;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
    public class VertexBuffer<TVertex>
        where TVertex : struct, IVertex // _vertices must be structs so we can copy them to GPU memory easily
    {
        public int VertexCount => _count;
        public int VertexSize => _vertexSize;
        
        private int _vertexSize;

        private TVertex[] _vertices = new TVertex[4];

        private int _count;

        private readonly int _adress;

        public VertexBuffer(params TVertex[] vertices)
        {
            foreach (var vertex in vertices)
            {
                AddVertex(vertex);
            }
            
            // generate the actual Vertex Buffer Object
            _adress = GL.GenBuffer();
        }

        public TVertex Get(int index)
        {
            return _vertices[index];
        }

        public void ResetVertices()
        {
            _vertices = new TVertex[4];
            _count = 0;
        }

        public void AddVertex(TVertex v)
        {
            if (_count <= 0)
            {
                _vertexSize = System.Runtime.InteropServices.Marshal.SizeOf(v);
            }

            // resize array if too small
            if(_count == _vertices.Length)
                Array.Resize(ref _vertices, _count * 2);
            // add vertex
            _vertices[_count] = v;
            _count++;
        }

        public void Bind()
        {
            // make this the active array buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, _adress);
        }

        public void BufferData()
        {
            // copy contained _vertices to GPU memory
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(_vertexSize * _count),
                _vertices, BufferUsageHint.StreamDraw);
        }

        public void Draw()
        {
            // draw buffered _vertices as triangles
            GL.DrawArrays(PrimitiveType.Triangles, 0, _count);
        }
    }
}
