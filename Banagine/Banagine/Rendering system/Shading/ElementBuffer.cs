﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
    public class ElementBuffer
    {
        private readonly int _adress;

        public int[] Indices { get; set; }

        public ElementBuffer()
        {
            _adress = GL.GenBuffer();
        }

        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _adress);
        }

        public void BufferData()
        {
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(System.Runtime.InteropServices.Marshal.SizeOf(Indices[0]) * Indices.Length), Indices, BufferUsageHint.StreamDraw);
        }

        public void Draw()
        {
            GL.DrawElements(PrimitiveType.Triangles, Indices.Length, DrawElementsType.UnsignedInt, 0);
        }
    }
}