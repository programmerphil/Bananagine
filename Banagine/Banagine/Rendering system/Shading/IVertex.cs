﻿namespace Banagine.Rendering_system.Shading
{
    public interface IVertex
    {
        OpenTK.Vector2 Position { get; set; }
    }
}