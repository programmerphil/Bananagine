﻿using OpenTK;
using OpenTK.Graphics;

namespace Banagine.Rendering_system.Shading
{
    public struct ColouredVertex : IVertex
    {
        public Vector2 Position { get; set; }
        public const int Size = (2 + 4) * 4;

        private readonly Color4 _color;

        public ColouredVertex(Vector2 position, Color4 color)
        {
            Position = position;
            _color = color;
        }
    }
}
