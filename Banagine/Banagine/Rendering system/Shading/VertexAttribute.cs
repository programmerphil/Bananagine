﻿using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
    public class VertexAttribute
    {
        private readonly string _name;
        private readonly int _size;
        private readonly VertexAttribPointerType _type;
        private readonly bool _normalize;
        private readonly int _stride;
        private readonly int _offset;

        public VertexAttribute(string name, int size, VertexAttribPointerType type,
            int stride, int offset, bool normalize = false)
        {
            _name = name;
            _size = size;
            _type = type;
            _stride = stride;
            _offset = offset;
            _normalize = normalize;
        }

        public void Set(ShaderProgram program)
        {
            // get location of attribute from shader program
            int index = program.GetAttribute(_name);
            
            // enable and set attribute
            GL.EnableVertexAttribArray(index);
            
            GL.VertexAttribPointer(index, _size, _type,
                _normalize, _stride, _offset);
            System.Console.WriteLine("Attribute " + _name + " Index " + index + " Error " + GL.GetError());

        }
    }
}
