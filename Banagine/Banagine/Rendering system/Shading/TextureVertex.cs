﻿using OpenTK;
using OpenTK.Graphics;

namespace Banagine.Rendering_system.Shading
{
    public struct TextureVertex : IVertex
    {
        public const int Size = (2 + 4 + 2) * 4;

        public Vector2 Position { get; set; }
        public Color4 Color;
        public Vector2 TextureCord;

        public TextureVertex(Color4 color, Vector2 textureCord, Vector2 position)
        {
            Color = color;
            TextureCord = textureCord;
            Position = position;
        }
    }
}