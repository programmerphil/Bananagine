﻿using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
    public class VertexArray<TVertex>
        where TVertex : struct, IVertex
    {
        private readonly int _handle;

        public VertexArray(VertexBuffer<TVertex> vertexBuffer,ElementBuffer elementBuffer, ShaderProgram program,
            params VertexAttribute[] attributes)
        {
            // create new vertex array object
            GL.GenVertexArrays(1, out _handle);
            
            // bind the object so we can modify it
            Bind();

            // bind the vertex buffer object
            elementBuffer.Bind();
            vertexBuffer.Bind();
           
            // set all attributes
            foreach (var attribute in attributes)
            {
                attribute.Set(program);
            }
            
            // unbind objects to reset state
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            
        }

        public void Bind()
        {
            // bind for usage (modification or rendering)
            GL.BindVertexArray(_handle);
        }
    }
}
