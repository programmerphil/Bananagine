﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
    public class Matrix4Uniform
    {
        private readonly string _name;
        private Matrix4 _matrix;

        public Matrix4 Matrix
        {
            get { return _matrix; } set { _matrix = value; }
        }

        public Matrix4Uniform(string name, Matrix4 matrix)
        {
            _name = name;
            _matrix = matrix;}

        public void Set(ShaderProgram program)
        {
            // get uniform location
            var i = program.GetUniform(_name);

            // set uniform value
            GL.UniformMatrix4(i, false, ref _matrix);
        }
    }
}
