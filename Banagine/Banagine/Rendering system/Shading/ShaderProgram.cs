﻿using OpenTK.Graphics.OpenGL;

namespace Banagine.Rendering_system.Shading
{
    public class ShaderProgram
    {
        public int ProgramId { get; private set; }

        public ShaderProgram(params ShaderData[] shadersData)
        {
            ProgramId = GL.CreateProgram();

            foreach (var shaderData in shadersData)
            {
                GL.AttachShader(ProgramId, shaderData.Address);
            }

            Link();

            foreach (var shaderData in shadersData)
            {
                GL.DetachShader(ProgramId, shaderData.Address);
            }
        }

        private void Link()
        {
            GL.LinkProgram(ProgramId);
        }
        
        public int GetUniform(string name)
        {
            return GL.GetUniformLocation(ProgramId, name);
        }

        public int GetAttribute(string name)
        {
            return GL.GetAttribLocation(ProgramId, name);
        }

        public void Use()
        {
            GL.UseProgram(ProgramId);
        }
    }
}