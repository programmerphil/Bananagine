﻿using System;
using Banagine.EC;
using Banagine.EventSystem;
using Banagine.Rendering_system.Drawables;
using Banagine.Rendering_system.OpenTk;
using Banagine.Scene_system;

namespace Banagine.Rendering_system
{
    public class Window
    {
        public void Render()
        {
            EventManager.SendMessage(this, EngineEvent.Render, EventArgs.Empty, true);
        }

        public void Update()
        {
            EventManager.SendMessage(this, EngineEvent.EarlyUpdate, EventArgs.Empty, true);
            EventManager.SendMessage(this, EngineEvent.Update, EventArgs.Empty, true);
            EventManager.SendMessage(this, EngineEvent.LateUpdate, EventArgs.Empty, true);
        }
    }
}