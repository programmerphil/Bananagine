﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Banagine.ExtensionsMethods;
using Microsoft.Build.Evaluation;

namespace Pipeline
{
    public class AssetFolderPath : IFolderPath
    {
        
        private string _assetPath;
        private FolderPath _folderPath;

        public List<string> FilePaths => _folderPath.FilePaths;
        public string[] FileExtensions => _folderPath.FileExtensions;
        public string PathToFolder => _folderPath.PathToFolder;
        public string AssetPath => _assetPath;
        public string ProjectPath { get; }

        public string RelativeFolderPath => PathToFolder.Replace(ProjectPath + @"\", "");

        public string FolderName { get; }

        public AssetFolderPath(string projectPath, string assetPath, string folderName, params string[] allowableFileExtensions)
        {
            _assetPath = assetPath;
            FolderName = folderName;
            ProjectPath = projectPath;
            _folderPath = new FolderPath(projectPath + @"\" + assetPath + @"\" + folderName, allowableFileExtensions);
        }

        public void LoadFilePaths()
        {
            _folderPath.LoadFilePaths();
        }
    }
}