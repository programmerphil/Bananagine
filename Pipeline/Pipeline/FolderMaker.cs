﻿using System.IO;

namespace Pipeline
{
    public class FolderMaker
    {
        private string _folderName;

        public FolderMaker(string folderName)
        {
            _folderName = folderName;
        }

        public void MakeFolderIfNotExist(string path)
        {
            string folderPath = path + @"\" + _folderName;

            if (Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
        }
    }
}