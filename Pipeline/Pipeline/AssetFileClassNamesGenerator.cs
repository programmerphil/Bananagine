﻿using System;
using System.IO;
using System.Text;
using Banagine.ExtensionsMethods;
using Microsoft.Build.Evaluation;

namespace Pipeline
{
    public class AssetFileClassNamesGenerator
    {
        private AssetFolderPath _assetFolderPath;
        private string _className;
        private string _nameSpace;

        public AssetFileClassNamesGenerator(AssetFolderPath assetFolderPath, string className, string nameSpace)
        {
            _assetFolderPath = assetFolderPath;
            _className = className;
            _nameSpace = nameSpace;
        }

        public void Generate(Project project, string projectPath)
        {
            string classPath = _assetFolderPath.PathToFolder + @"\" + _className + ".cs";

            CreateClass(GenerateClass(), classPath);

            ProjectItem matchingProjectItem = null;

            foreach (var content in project.GetItems("Compile"))
            {
                string relativeClassPath = classPath.Replace(projectPath + @"\", "");
                if (content.UnevaluatedInclude.Contains(relativeClassPath))
                {
                    matchingProjectItem = content;
                }
            }

            if (matchingProjectItem == null)
            {
                project.AddItem("Compile", classPath);
                project.Save();
            }
        }

        private void CreateClass(string classText, string classPath)
        {
            File.WriteAllText(classPath, classText);
        }

        private string GenerateClass()
        {
            string generatedClass = "namespace " + _nameSpace + Environment.NewLine + "{" + Environment.NewLine +  "    public class " + _className + Environment.NewLine + "    {" + Environment.NewLine;

            foreach (var filePath in _assetFolderPath.FilePaths)
            {
                string fileName = filePath.GetFileName();
                string fileNameWithNoSpaces = fileName.Replace(" ", "_");
                generatedClass += "        public const string " + fileNameWithNoSpaces + " = \"" + fileName + "\";" + Environment.NewLine;
            }

            generatedClass += "    }" + Environment.NewLine + "}";

            return generatedClass;
        }
    }
}