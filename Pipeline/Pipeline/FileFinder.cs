﻿using System;
using System.Collections.Generic;
using System.IO;
using Banagine.ExtensionsMethods;

namespace Pipeline
{
    public class FileFinder
    {
        private List<string> _allowableFileExtensions;

        public FileFinder(List<string> allowableFileExtensions)
        {
            _allowableFileExtensions = allowableFileExtensions;
        }

        public List<string> GetFilesWithAllowableFileExtension(string folderPath)
        {
            List<string> filesWithAllowableFileExtension = new List<string>();

            foreach (var fileName in Directory.GetFiles(folderPath, "*.*", SearchOption.AllDirectories))
            {
                if (_allowableFileExtensions.Exists(item => string.Equals(fileName.GetFileExtension(), item, StringComparison.CurrentCultureIgnoreCase)))
                {
                    filesWithAllowableFileExtension.Add(fileName);
                }
            }

            return filesWithAllowableFileExtension;
        }
    }
}