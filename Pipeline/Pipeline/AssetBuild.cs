﻿using System.Collections.Generic;
using System.IO;
using Banagine.ExtensionsMethods;

namespace Pipeline
{
    public class AssetBuild
    {
        private string _buildDirectory;

        public AssetBuild(string buildDirectory)
        {
            _buildDirectory = buildDirectory;
        }

        public void Build(AssetFolderPath assetFolderPath)
        {
            string relativeFolderPath = assetFolderPath.RelativeFolderPath;
            Directory.CreateDirectory(_buildDirectory + @"\" + relativeFolderPath);
            foreach (var filePath in assetFolderPath.FilePaths)
            {
                Build(filePath, _buildDirectory + @"\" + relativeFolderPath + @"\" + filePath.GetFileNameWithExtension());
            }
        }

        public void Build(IFolderPath folderPath)
        {
            foreach (var filePath in folderPath.FilePaths)
            {
                Build(filePath, _buildDirectory + @"\" + filePath.GetFileNameWithExtension());
            }
        }

        public void Build(string filePath, string buildpath)
        {
            File.Copy(filePath, buildpath, true);
        }
    }
}