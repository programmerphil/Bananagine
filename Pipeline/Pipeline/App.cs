﻿using System;
using System.IO;
using System.Linq;
using Banagine.ExtensionsMethods;
using EnvDTE;
using EnvDTE80;
using Microsoft.Build.Evaluation;
using Project = Microsoft.Build.Evaluation.Project;
using ProjectItem = Microsoft.Build.Evaluation.ProjectItem;

namespace Pipeline
{
    public class App
    {
        private static IFolderPath[] _folderPaths;
        private static AssetBuild _assetBuild;
        private static string _projectName;
        private static Project _project;

        static void Main(string[] args)
        {
            _projectName = args[0];
            string projectPath = args[1];
            string projectFile = args[2];
            string buildDirectory = args[3];
            _assetBuild = new AssetBuild(buildDirectory);

            var collection = new ProjectCollection();
            collection.DefaultToolsVersion = "4.0";

            _project = new Project(projectFile, null, "4.0");

            LoadAsssetFilePaths(projectPath);

            foreach (var folderPath in _folderPaths)
            {
                Build(folderPath);
            }
        }

        private static void Build(AssetFolderPath assetFolderPath)
        {
            assetFolderPath.LoadFilePaths();
            _assetBuild.Build(assetFolderPath);
            AssetFileClassNamesGenerator assetFileClassNamesGenerator = new AssetFileClassNamesGenerator(assetFolderPath, assetFolderPath.FolderName, _projectName + "." + assetFolderPath.RelativeFolderPath.Replace(@"\", "."));
            assetFileClassNamesGenerator.Generate(_project, assetFolderPath.ProjectPath);
        }

        private static void Build(FolderPath folderPath)
        {
            folderPath.LoadFilePaths();
            _assetBuild.Build(folderPath);
        }

        private static void Build(IFolderPath folderPath)
        {
            CallIfCanCast<FolderPath>(folderPath, Build);
            CallIfCanCast<AssetFolderPath>(folderPath, Build);
        }

        private static void CallIfCanCast<T>(object objectToCast, Action<T> actionOnSuccesCast) where T : class
        {
            T casted = objectToCast as T;
            if (casted != null)
            {
                actionOnSuccesCast?.Invoke(casted);
            }
        }

        private static void LoadAsssetFilePaths(string projectPath)
        {
            _folderPaths = new IFolderPath[]
            {
                FolderPaths.CreateShaderAssetFilePath(projectPath),
                FolderPaths.CreateTexureAssetFilePath(projectPath),
                FolderPaths.CreateSoundAssetFilePath(projectPath)
            };
        }
    }
}