﻿namespace Pipeline
{
    public class FolderPaths
    {
        public static AssetFolderPath CreateShaderAssetFilePath(string projectPath)
        {
            AssetFolderPath assetFolderPath = new AssetFolderPath(projectPath, "Assets", "Shaders",  "vertexShader", "fragmentShader");
            assetFolderPath.LoadFilePaths();
            return assetFolderPath;
        }

        public static AssetFolderPath CreateTexureAssetFilePath(string projectPath)
        {
            AssetFolderPath assetFolderPath = new AssetFolderPath(projectPath, "Assets", "Textures", "png", "jpg", "bmp", "exif", "tiff");
            assetFolderPath.LoadFilePaths();
            return assetFolderPath;
        }

        public static AssetFolderPath CreateSoundAssetFilePath(string projectPath)
        {
            AssetFolderPath assetFolderPath = new AssetFolderPath(projectPath, "Assets", "Audio", "wav", "ogg", "mp3", "flac", "mod", "it", "s3d", "xm");
            assetFolderPath.LoadFilePaths();
            return assetFolderPath;
        }
    }
}