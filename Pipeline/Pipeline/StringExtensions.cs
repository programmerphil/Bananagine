﻿using System;

namespace Banagine.ExtensionsMethods
{
    public static class StringExtensions
    {
        public static string GetFileExtension(this string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf(".", StringComparison.Ordinal) + 1);
        }

        public static string GetFileName(this string filePath)
        {
            int fileNameStartIndex = filePath.LastIndexOf(@"\", StringComparison.Ordinal);
            int fileNameEndIndex = filePath.LastIndexOf(".", StringComparison.Ordinal);
            return filePath.Substring(fileNameStartIndex + 1, (fileNameEndIndex - fileNameStartIndex) - 1);
        }

        public static string GetFolderName(this string folderPath)
        {
            int folderNameStartIndex = folderPath.LastIndexOf(@"\", StringComparison.Ordinal);
            return folderPath.Substring(folderNameStartIndex);
        }

        public static string GetFileNameWithExtension(this string filePath)
        {
            return filePath.GetFileName() + "." + filePath.GetFileExtension();
        }
    }
}