﻿using System.Collections.Generic;

namespace Pipeline
{
    public interface IFolderPath
    {
        List<string> FilePaths { get; }
        string FolderName { get; }
        string[] FileExtensions { get; }
        string PathToFolder { get; }

        void LoadFilePaths();
    }
}