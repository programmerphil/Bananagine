﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Banagine.ExtensionsMethods;

namespace Pipeline
{
    public class FolderPath : IFolderPath
    {
        private FolderMaker _folderMaker;
        private FileFinder _fileFinder;

        public string PathToFolder { get; private set; }
        public List<string> FilePaths { get; private set; }
        public string FolderName { get; private set; }
        public string[] FileExtensions { get; private set; }

        public FolderPath(string pathToFolder, params string[] fileExtensions)
        {
            FolderName = pathToFolder.GetFolderName();
            FileExtensions = fileExtensions;
            _folderMaker = new FolderMaker(FolderName);
            _fileFinder = new FileFinder(FileExtensions.ToList());
            PathToFolder = pathToFolder;
            Directory.CreateDirectory(PathToFolder);
        }

        public void LoadFilePaths()
        {
            _folderMaker.MakeFolderIfNotExist(PathToFolder);
            FilePaths = _fileFinder.GetFilesWithAllowableFileExtension(PathToFolder);
        }
    }
}